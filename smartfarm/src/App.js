import React, {Component} from 'react';
import Sidebar from './Components/Sidebar/Sidebar'
import PetIcon from "@material-ui/icons/Pets";
import GroupIcon from '@material-ui/icons/Group';
import SearchIcon from '@material-ui/icons/Search';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import AssessmentIcon from '@material-ui/icons/Assessment';
import FitnessCenterIcon from '@material-ui/icons/FitnessCenter';
import ViewListIcon from '@material-ui/icons/ViewList';
import CompareArrowsIcon from '@material-ui/icons/CompareArrows';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Navigationbar } from './Components/Navbar/Navbar';
import Logout from './Components/Logout/Logout';
import Cow from './Components/Cow/Cow';
import Collar from './Components/Collar/Collar'; 
import Weight from './Components/Weight/Weight';
import Login from './Components/Login/Login';
import UploadDataFile from './Components/UploadDataFile/UploadDataFile';
import LookUpCow from './Components/LookUpCow/LookUpCow';
import CowDayDisplay from './Components/LookUpCow/CowDayDisplay/CowDayDisplay';
import CowRangeDisplay from './Components/LookUpCow/CowRangeDisplay/CowRangeDisplay';
import CompareCow from './Components/CompareCow/CompareCow';
import CompareCowRangeDisplay from './Components/CompareCow/CompareCowRangeDisplay/CompareCowRangeDisplay';
import CompareCowDayDisplay from './Components/CompareCow/CompareCowDayDisplay/CompareCowDayDisplay';
import LookUpHerd from './Components/LookUpHerd/LookUpHerd'
import LookupHerdRangeDisplay from './Components/LookUpHerd/LookUpHerdRangeDisplay/LookUpHerdRangeDisplay';
import LookupHerdDayDisplay from './Components/LookUpHerd/LookUpHerdDayDisplay/LookUpHerdDayDisplay';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Report from './Components/Report/Report';
import Registry from './Components/Registry/Registry';


const sideBarMenuItems = [
  { name: 'herd', label: 'Herd Details', Icon: PetIcon, navigateTo: '/Cow' },
  { name: 'lookupcow', label: 'Look up Cow', Icon: SearchIcon, navigateTo: '/LookUpCow' },
  { name: 'lookuphear', label: 'Look up Herd', Icon: SearchIcon, navigateTo: '/LookUpHerd' },
  { name: 'compareCow', label: 'Compare Cows', Icon: CompareArrowsIcon, navigateTo: '/CompareCow' },
  { name: 'collar', label: 'Collar Inventory', Icon: ViewListIcon, navigateTo: '/Collar' },
  { name: 'weight', label: 'Weight Management', Icon: FitnessCenterIcon, navigateTo: '/Weight' },
  { name: 'upload', label: 'Upload Data File', Icon: FileCopyIcon, navigateTo: '/UploadFile' }, 
  { name: 'report', label: 'Reports', Icon: AssessmentIcon, navigateTo: '/Report' }, 

  "divider",
  { name: 'users', label: 'User Management', Icon: GroupIcon, navigateTo: '/Registry' },
];

class App extends Component {

  render () {
    return (
      <React.Fragment>
        <Router>
          <div>          
          <Navigationbar />
          <Sidebar menuItems={sideBarMenuItems} />
          <Switch>
            <Route exact path="/" component={Login} />
            <Route path="/Logout" component={() => <Logout/>} />
            <Route path="/Cow/" component={() => <Cow/>} />
            <Route path="/UploadFile/" component={ UploadDataFile } />
            <Route path="/LookUpCow" component={ LookUpCow } />
            <Route path="/LookUpHerd" component={ LookUpHerd } />
            <Route path="/CompareCow" component={ CompareCow } />
            <Route path="/Collar/" component={() => <Collar/>} />
            <Route path="/Weight/" component={() => <Weight/>} />
            <Route path="/Report/" component={() => <Report/>} />
            <Route path="/CowRangeDisplay/:cowId" component={ CowRangeDisplay } />
            <Route path="/CowDayDisplay/:cowId" component={ CowDayDisplay } />
            <Route path="/CompareCowRangeDisplay" component={ CompareCowRangeDisplay } />
            <Route path="/CompareCowDayDisplay" component={ CompareCowDayDisplay } />
            <Route path='/Registry' component={() => <Registry/>} />
            <Route path="/LookupHerdRangeDisplay" component={ LookupHerdRangeDisplay } />
            <Route path="/LookupHerdDayDisplay" component={ LookupHerdDayDisplay } />
          </Switch>
          </div>
        </Router>
      </React.Fragment>
    )
  }
  
}

export default App