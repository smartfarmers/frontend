import React from 'react'
import './ErrorPopup.css';

class Popup extends React.Component {
    render() {
      return (
        <div classname='popup-wrapper'>
          <div className='popup'>
            <div className='popup_inner' >
              <h1>{this.props.text}</h1>
            <button type="confirm" class={"OKButton"} onClick={this.props.closePopup}>OK</button>
            </div>
          </div>
        </div>
      );
    }
  }

export default Popup