import React, { Fragment } from 'react' 
import './AllocateCollarPopup.css';  
import Error from '../../Errors/Error'
import Popup from './ErrorPopup'

class AllocatePopup extends React.Component {
    
    constructor(props) {
		super(props);
		this.state = {
          isLoaded: false,
          collarAllocateResponse: [],
          getError: null,
          showPopup: false,
          showAdded: false
		}
        this.handleChange = this.handleChange.bind(this)
        this.setAllocation = this.setAllocation.bind(this)
        this.handleOK = this.handleOK.bind(this)
        this.handleCancel = this.handleCancel.bind(this)
        this.getCollarList = this.getCollarList.bind(this)
        this.orderCollars = this.orderCollars.bind(this)
        this.togglePopup = this.togglePopup.bind(this)
        this.toggleAdded = this.toggleAdded.bind(this) 
	  }

    componentDidMount(){
        this.getCollarList()        
    }

    async getCollarList() {
        await fetch(`${process.env.REACT_APP_API_URL}/allocate`, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}})
		.then(res => res.json())
           .then(json => {
            let collars = this.orderCollars(json.response)
            this.setState({
              isLoaded: true,  
              collarList: collars
            })
        }).catch(error => {
			this.setState({
			  getError: error
			})
		});
    }

    orderCollars(response) {
        let collars = [];
        var id = 0;

        response.forEach(element => {
            let item = {
                id: id,
                collar_id: element["COLLAR_ID"],
                collSerNo: element["COLLAR_SER_NO"],
                collNum: element["COLLAR_NUM"],
                dateOfEntry: element["DATE_OF_ENTRY"],
                collarLoc: element["collar_loc_id"],
                dateOn: element["date_on"],
                dateOff: element["date_off"]
            }
            if (item.collarLoc === null) {
              collars.push(item)
              id++ 
            } else if (item.dateOff !== null && collars.length > 0) {
                if (item.collar_id === collars[collars.length-1].collar_id) {
                    collars.pop()
                }                    
                collars.push(item)
                id++                   
            } else if (item.dateOff === null && collars.length > 0){
                if (item.collar_id === collars[collars.length-1].collar_id) {
                    collars.pop()
                }
            } else if (item.dateOff !== null && collars.length === 0) {
                collars.push(item)
                id++  
            }             
        }) 
        return(collars)
    }
      
    handleChange(event){
        this.setState ({
            collId: event.target.value})                  
        }

    handleOK(event){
        var collarId = this.state.collId
        this.setAllocation(collarId)
        this.setState({
            OKclicked: true
        })
    }

    handleCancel(event){
        this.props.toggle();
    }


    async setAllocation(collar_id){
		let body = '{ "cow_id": "'+this.props.cowId+'", "collar_id": "'+collar_id+'" }';
		await fetch(`${process.env.REACT_APP_API_URL}/allocate`, {
			method: 'post',
			headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
			body: body
		}).then(res => res.json())
        .then(json => {           
            this.getCollarList()
            this.setState({
                collarAllocateResponse: json,
                showAdded: true
            })      
        })
        .catch(error => {
			this.setState({
                getError:error,
                showPopup: true
			})
		});
	}

    getError(){
		if(this.state.getError){
		  return (
			<Error
			  errorType="error"
			  errorHeading="Unable to reach database" 
			  errorMessage="We were unable to update collar allocation, please contact application administrator">
			</Error>
		  )
		}
	  }

    togglePopup() {
        this.getCollarList()
        this.props.reload()
        this.props.toggle()
        this.setState({
            showPopup: !this.state.showPopup
        });
    }

    toggleAdded() {
        this.getCollarList()
        this.props.reload()
        this.props.toggle()
    }

    render() { 
    var {isLoaded } = this.state;
    if (!isLoaded) {
      return <div>Loading...</div>;
    } else { 
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
        return ( 
            <Fragment>
                <card>
                    <h6>Allocate Collar to Cow:</h6>
                    <input type="text" name="Cow ID" value={this.props.cowId}  disabled={true}/> 
                    <h6>Collar id:</h6> 
                    <select type="allocate-select"onChange={this.handleChange}>
                    <option value="" disabled selected>Select collar</option>
                    { this.state.collarList.map((collars) => <option key={collars.id} value={collars.collar_id}> 
                        Collar Number: {collars.collNum} Collar ID: {collars.collar_id}</option>) }
                    </select>
                    <button type="cow-submit" onClick={this.handleOK} disabled={Boolean(this.state.OKclicked)}> Add</button>&nbsp;
                    <button type="cow-submit" onClick={this.handleCancel}>Cancel </button>
                </card>
                <Fragment>
                    {this.state.showPopup ? 
                        <Popup
                            text='Unable to update collar allocation, please contact application administrator'
                            closePopup={this.togglePopup.bind(this)}
                        /> : null
                    }
                </Fragment>
                <Fragment>
                    {this.state.showAdded ? 
                        <Popup
                            text='Collar was successfully allocated'
                            closePopup={this.toggleAdded.bind(this)}
                        /> : null
                    }
                </Fragment>
            </Fragment>
                        
        )};  
    }  
}  

export default AllocatePopup;