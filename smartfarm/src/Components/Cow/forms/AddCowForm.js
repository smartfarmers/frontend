import React, { useState } from 'react'

const AddCowForm = props => {
    const initialFormState = { id: null, cow_id: '', dateOfBirth: '', dateOfEntry: '' , dateOfExit: ''}
	const [ cow, setCow ] = useState(initialFormState)

	const handleInputChange = event => {
        const { name, value } = event.target
		setCow({ ...cow, [name]: value })
	}

	return (
		<form
			onSubmit={event => {
				event.preventDefault()
				if (!cow.cow_id || !cow.dateOfBirth ) {
					window.alert('Enter both Cow ID & Date of Birth to add a new entry')
					setCow(initialFormState)				
				} else {
					props.addCow(cow)
					setCow(initialFormState)
				}				
			}}
		>
			<h6>Cow ID</h6>
			<input type="cow-text" name="cow_id" value={cow.cow_id} onChange={handleInputChange} pattern="[A-Za-z0-9-]+" placeholder="M562"/>

			<h6>Date of Birth</h6>
			<input type="date" name="dateOfBirth" value={cow.dateOfBirth} onChange={handleInputChange} placeholder="2020-03-03"/>
	
			<button type="cow-submit">Add new cow</button>
			<p></p>
		</form>
	)
}

export default AddCowForm