import React, { useState, useEffect } from 'react'

const EditCowForm = props => {
  const [ cow, setCow ] = useState(props.currentCow)

  useEffect(
    () => {
      setCow(props.currentCow)
    },
    [ props ]
  )

  const handleInputChange = event => {
    const { name, value } = event.target

    setCow({ ...cow, [name]: value })
  }

  return (
    <form
      onSubmit={event => {
        event.preventDefault()
        console.log(cow)
        props.updateCow(cow.id, cow)
      }}
    >
      <h6>Cow ID</h6>
      <input type="cow-text" name="cow_id" value={cow.cow_id} onChange={handleInputChange} disabled={true}/>

      <h6>Date of Birth</h6>
      <input type="date" name="dateOfBirth" value={cow.dateOfBirth} onChange={handleInputChange} />

      <h6>Date of Entry</h6>
      <input type="date" name="dateOfEntry" value={cow.dateOfEntry} onChange={handleInputChange} />

      <h6>Date of Exit</h6>
      <input type="date" name="dateOfExit" value={cow.dateOfExit} onChange={handleInputChange} />
      
      <button type="cow-submit">Update cow</button>&nbsp;
      {() => props.addCow(cow)}
      <button type="cow-submit" onClick={() => { 
        window.location.reload()}}>Cancel</button>
       
    </form>
  )
}

export default EditCowForm
