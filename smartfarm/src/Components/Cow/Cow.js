import React, { Fragment, Component } from 'react'
import AddCowForm from './forms/AddCowForm'
import EditCowForm from './forms/EditCowForm'
import CowTable from './tables/CowTable'
import AllocatePopup from './popups/AllocateCollarPopup'
import Error from '../Errors/Error'
import './Cow.css'
import { Redirect } from 'react-router-dom'; 
import Popup from './popups/ErrorPopup'

// Setting state
class Cow extends Component {

	constructor(props) {
		super(props);
		this.state = {
		  cowData: [],
		  cowGetResponse: [],
		  cowPostResponse: null,
		  collCow: '',
		  cowPostResponse: null,
		  currentCow: null,
		  editing: null,
		  setEditing: '',
		  showAllocateCollarPopup: false,
		  getError: null,
		  postError: null,
		  putError: null,
		  deleteError: null,
		  retireError:null,
		  allocated: null,
		  hideAddCow: false,
		  showPopup: false,
		  showAdded: false,
		  addClicked: false		  	
		}
		this.addCow = this.addCow.bind(this)
		this.deleteCow = this.deleteCow.bind(this)
		this.editRow = this.editRow.bind(this)
		this.retireCow = this.retireCow.bind(this)
		this.updateCow = this.updateCow.bind(this)
		this.mapGetCowResponse = this.mapGetCowResponse.bind(this)
		this.mapNewCow = this.mapNewCow.bind(this)
		this.getCowList = this.getCowList.bind(this)
		this.mapUpdateCow = this.mapUpdateCow.bind(this)
		this.allocateCollar = this.allocateCollar.bind(this)
		this.removeCollar = this.removeCollar.bind(this)
		this.toggleAllocateCollarPopup = this.toggleAllocateCollarPopup.bind(this)
		this.collarGone = this.collarGone.bind(this)
		this.checkCollarStatus = this.checkCollarStatus.bind(this)
		this.togglePopup = this.togglePopup.bind(this)
		this.toggleAdded = this.toggleAdded.bind(this)
		this.closeEdit = this.closeEdit.bind(this) 
	  }

	  componentDidMount(){		  
		this.getCowList()
	  }

	  	getCowList(){
		//fetches cow list
		fetch(`${process.env.REACT_APP_API_URL}/cow/`, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) // fetches a list of cows and their current collar
       	.then(res => res.json())
       	.then(json => {
			if(json.status != 200){
				this.setState({
					cowGetResponse: json,
					getError: json.status
			})
			} else {
				let mappedResponse = this.mapGetCowResponse(json.response)
				this.setState({
					cowGetResponse: json,
					cowData: mappedResponse
				})
			} 
		  })
		  .catch(error => {
			this.setState({
			  getError: error
			})
		});
	}

	  mapGetCowResponse(response) {
		let result = []
		var id = 0
		var canDelete = true;
		response.forEach(element => {
		  let item = {
			id: id,
			cow_id: element["COW_ID"],
			dateOfBirth: element["DATE_OF_BIRTH"],
			dateOfEntry: element["DATE_OF_ENTRY"],
			dateOfExit: element["DATE_OF_EXIT"],
			collarNum: element["collar_num"],
			collarId: element["collar_id"],
			dateOff: element["date_off"],
			canDelete: canDelete
		  }
		  if (item.collarNum != null){
			  item.canDelete = false
		  }
		  if (result.length > 0){
			if (item.cow_id === result[result.length-1].cow_id) {
				item.canDelete = false
				result.pop()
			}
		}
		  result.push(item)
		  id++
		});
		this.checkCollarStatus(result)
		return result
	  }

	  checkCollarStatus(response) {
		response.forEach(element => {
		  if (element.dateOff != null) {
			  element.collarNum = null
		  }
		})
	}

	// CRUD operations
	addCow(cow) {
		cow.id = this.state.cowData.length + 1
		this.setState({
			cowData: [ ...this.state.cowData, cow ]
		})

    let newCow = this.mapNewCow(cow);
    fetch(`${process.env.REACT_APP_API_URL}/cow`, {
      method: 'post',
      headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
      body: JSON.stringify(newCow)
    }).then(res => res.json())
      .then(json => {
		this.getCowList()  
        this.setState({
            cowPostResponse: json
      }, () => {
        if(json.status != 201 ){
          this.setState({
            postError: json.status
          })
		}
        })
    }).catch(error => {
		this.getCowList()
		this.setState({
		  postError: error
		})
	  });
  }

  mapNewCow(cow) {
    let item = {
		cow_id: cow["cow_id"],
      	date_of_birth: cow["dateOfBirth"]
    }
    return item
  }

  mapUpdateCow(cow) {
    let item = {
		date_of_birth: cow["dateOfBirth"],
		date_of_entry: cow["dateOfEntry"],
		date_of_exit: cow["dateOfExit"]
    }
    return item
  }

  deleteCow(cow) {
    fetch(`${process.env.REACT_APP_API_URL}/cow/` + cow["cow_id"], {
	  method: 'delete',
	  headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}
    }).then(res => res.json())
      .then(json => {
		if(json.status != 200 ){
			this.setState({
			  deleteError: json
			})
		  }
        this.setState({
            cowDeleteResponse: json,
      }, () => {
        this.getCowList()
      })
    }).catch(error => {
		this.setState({
		  deleteError: error
		})
	  });
	}

	retireCow(cow){
		fetch(`${process.env.REACT_APP_API_URL}/cow/` + cow["cow_id"], {
		  method: 'PATCH',
		  headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}
		}).then(res => res.json())
		  .then(json => {
			this.getCowList()
			this.setState({
				cowPatchResponse: json,
		  }, () => {
			this.getCowList()
		  })
		}).catch(error => {
			this.setState({
			  retireError: error
			})
		  });;
	}

	updateCow(id, updatedCow) {
		this.setState({
		  editing: false,
		  cowData: this.state.cowData.map(cow => (cow.id === id ? updatedCow : cow))
		})
	
		let mappedCow = this.mapUpdateCow(updatedCow);
		fetch(`${process.env.REACT_APP_API_URL}/cow/` + updatedCow.cow_id, {
		  method: 'put',
		  headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
		  body: JSON.stringify(mappedCow)
		}).then(res => res.json())
		  .then(json => {
			this.getCowList()
			this.setState({
				cowPutResponse: json,
				showAdded: true
		  }, () => {
			this.getCowList()
		  })
		}).catch(error => {
			this.setState({
				putError: error,
				showPopup: true
			})
		});
	}

	allocateCollar(cow_id) {
		let cowId = cow_id;
		this.toggleAllocateCollarPopup();
		this.getCowList()
		this.setState({
			collCow: cowId
		})
	}

	removeCollar(cow_id) {
		this.getCowList()
		let cowId = cow_id;
		fetch(`${process.env.REACT_APP_API_URL}/getcollar/` + cowId, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
       	.then(res => res.json())
       	.then(json => {
			let collarId = json.response;			
			let cId = collarId[0].collar_id
			this.collarGone(cId)
      	});
	}

	collarGone(collar_id) {
		fetch(`${process.env.REACT_APP_API_URL}/allocate/` + collar_id, {
		  method: 'PATCH',
		  headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}
		}).then(res => res.json())
		  .then(json => {
			this.getCowList()  
			this.setState({
				cowPatchResponse: json,
		  }, () => {
			this.getCowList()		
		  })
		});
	}

	toggleAllocateCollarPopup() {
		this.setState({  
			showAllocateCollarPopup: !this.state.showAllocateCollarPopup,
			allocated: true,
			hideAddCow: !this.state.hideAddCow
	   });
	   this.getCowList();  
	}
	
	editRow(cow) {
		this.setState({
		  editing: true,
		  currentCow: { id: cow.id, cow_id: cow.cow_id, dateOfBirth: cow.dateOfBirth, dateOfEntry: cow.dateOfEntry, dateOfExit: cow.dateOfExit }
		})
	}

	isAllocated(){
		if(this.state.allocated){
			this.getCollarList();
			this.setState({
				allocated: null
			})
		}
	}

	togglePopup() {
		this.getCowList()
        this.setState({
            showPopup: !this.state.showPopup
        });
    }

    toggleAdded() {
		this.getCowList()
		this.setState({
            showAdded: !this.state.showAdded
        });
    }

	closeEdit() {
		this.setState({
			editing: false
		})
	}

	getError(){
		if(this.state.getError){
		  return (
			<Error
			  errorType="error"
			  errorHeading="Unable to retrieve list of cows" 
			  errorMessage="We were unable to obtain the list of cows, please contact application administrator">
			</Error>
		  )
		}
	  }
	
	  putError(){
		if(this.state.putError){
		  return (
			<Error
			  errorType="error"
			  errorHeading="Unable to update cow details" 
			  errorMessage="We were unable to update cow, please contact application administrator">
			</Error>
		  )
		}
	  }
	
	  deleteError(){
		if (this.state.deleteError) {
		  if (this.state.deleteError.error != null ){
			if (this.state.deleteError.error.includes("Collar allocation exists")){
			  return (
				<Error
				  errorType="error"
				  errorHeading="Unable to delete cow due to a collar allocation" 
				  errorMessage="We were unable to delete cow, due to the cow being allocated. Please remove the allocation and try again.">
				</Error>
			  )
			}
		  }
		  return (
			<Error
			  errorType="error"
			  errorHeading="Unable to delete cow" 
			  errorMessage="We were unable to delete cow, please contact application administrator">
			</Error>
		  )
		}
	  }
	
	  postError(){
		if (this.state.postError) {
		  return (
			<Error
			  errorType="error"
			  errorHeading="Unable to create cow" 
			  errorMessage="We were unable to create cow, please contact application administrator">
			</Error>
		  )
		} 
	  }

	  retireError(){
		if(this.state.retireError){
		  return (
			<Error
			  errorType="error"
			  errorHeading="Unable to retire cow" 
			  errorMessage="We were unable to retire cow, please contact application administrator">
			</Error>
		  )
		}
	  }

	render() {
		if (!localStorage.getItem('token')) {
            return (
                <Redirect to={{ pathname: '/' }} push/>
            ) 
        } 
		return (
		  <div className="cow-container">
			<h1 className="CowHeading">Herd Details</h1>
			<div className="cow-flex-row">

			  <div className="cow-flex-large">
				{this.state.hideAddCow ? null : <Fragment>{this.state.editing ? (
				  <Fragment>
					<h2>Edit Cow</h2>
					<EditCowForm
					  editing={this.state.editing}
					  setEditing={this.state.setEditing}
					  currentCow={this.state.currentCow}
					  updateCow={this.updateCow}
					  close={this.closeEdit}
					/>
				  </Fragment>
				) : (
				  <Fragment>
					<h2>Add Cow</h2>
					<AddCowForm addCow={this.addCow} />
				  </Fragment>
				) 
				} </Fragment>}  								
			  </div>
			  
			  <div className="cow-flex-large">
				<div className="allocatePopup">
					<Fragment>
					<h2>Cow List</h2>
							{this.state.showAllocateCollarPopup ? 
						<AllocatePopup  cowId={this.state.collCow} toggle={this.toggleAllocateCollarPopup} reload={this.getCowList}/>
						: <CowTable cows={this.state.cowData} editRow={this.editRow} retireCow={this.retireCow} 
							deleteCow={this.deleteCow} allocateCollar={this.allocateCollar} removeCollar={this.removeCollar}/>
						}
					</Fragment>
				</div>
			  </div>
			<div className Popup>
			<Fragment>
                    {this.state.showPopup ? 
                        <Popup
                            text='Unable to update the cow details, please contact application administrator'
                            closePopup={this.togglePopup.bind(this)}
                        /> : null
                    }
                </Fragment>
                <Fragment>
                    {this.state.showAdded ? 
                        <Popup
                            text='Cow Details Updated'
							closePopup={this.toggleAdded.bind(this)}
							contentStyle={{width: "100px"}}
                        /> : null
                    }
                </Fragment></div>  
			<div className="flex-row cow-errors">
				{ this.getError() }
				{ this.putError() }
				{ this.postError() }
				{ this.deleteError() }
				{ this.retireError() }
        	</div>
			</div>
		  </div>
		) 
	  }
	}

 export default Cow;