import React from 'react'

const CowTable = props => (


  

  <table className = "cowTable">
    <thead className = "cowHead">
      <tr className = "cowTr">
        <th className = "cowTh"><h6>Cow ID</h6></th>
        <th className = "cowTh"><h6>Date of Birth</h6></th>
        <th className = "cowTh"><h6>Date of Entry</h6></th>
        <th className = "cowTh"><h6>Date of Exit</h6></th>
        <th className = "cowTh"><h6>Collar Number</h6></th>
      </tr>
    </thead>
    <tbody>
      {props.cows.length > 0 ? (
        props.cows.map(cow => (
          <tr key={cow.id}>
            <td className = "cowTd">{cow.cow_id}</td>
            <td className = "cowTd">{cow.dateOfBirth}</td>
            <td className = "cowTd">{cow.dateOfEntry}</td>
            <td className = "cowTd">{cow.dateOfExit}</td>
            <td className = "cowTd">{cow.collarNum}</td>  
            <td className = "cowTd">
            
                <button type="cow-submit"onClick={() => {
                  props.editRow(cow)}}>Edit </button>&nbsp;

                <button type="cow-submit" onClick={() => {
                  if (window.confirm('Are you sure you want to retire this cow?')) props.retireCow(cow)}}
                  disabled={Boolean(cow.collarNum) || Boolean(cow.dateOfExit)}>Retire Cow</button>&nbsp;

                <button type="cow-submit" onClick={() => { 
                  if (window.confirm('Are you sure you want to delete this cow?')) props.deleteCow(cow)}}
                  disabled={!Boolean(cow.collarNum === null) || Boolean(cow.canDelete === false)}
                  >Delete </button>&nbsp;&nbsp;&nbsp;
            
                <button type="cow-submit" onClick={() => 
                  props.allocateCollar(cow.cow_id) } 
                  disabled={!Boolean(cow.collarNum === null) || Boolean(cow.dateOfExit)}>Allocate Collar</button>&nbsp;
                
                <button type="cow-submit"
                  onClick={() => { 
                    if (window.confirm('Are you sure you want to remove this collar?')) props.removeCollar(cow.cow_id)}}
                    disabled={Boolean(cow.collarNum === null)}> Remove Collar</button>
                    
            </td>
          </tr>
        ))
      ) : (
        <tr>
          <td className = "cowTd" colSpan={3}>No Cows...</td>
        </tr>
      )}
    </tbody>
  </table>
)

export default CowTable