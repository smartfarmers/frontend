import React from 'react';
import { Nav, Navbar, Form, FormControl } from 'react-bootstrap';

import './Navbar.css';

export const Navigationbar = () => (
    <Navbar expand="lg" bg="dark">
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
                <Nav.Item><Nav.Link href="/logout">Logout</Nav.Link></Nav.Item>
            </Nav>
        </Navbar.Collapse>
    </Navbar>
)