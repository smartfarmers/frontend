import React from "react";
import './CowBattDay.css';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,} from 'recharts';

class CowBattDay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      battDay: [],
      isLoaded: false,
      cowId: '',
      date:''
    }
  }

  // fetches battery with cow_id for a specified date
  componentDidMount(){
    fetch(`${process.env.REACT_APP_API_URL}/battdate/` + this.props.cowId + '/'+ this.props.date, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
      .then(res => res.json())
      .then(json => {
       this.setState({
         isLoaded: true,
         battDay: json, 
       }) 
    }); 
  }
  
  render() {
    var {isLoaded, battDay } = this.state;
    if (!isLoaded) {
      return <div className="LookUpCowLoading">Loading...</div>;
    } else if(battDay.response.length === 0){
     return (
      <div className="LookUpCowNoData">No Battery data for this date</div>
     )
    }
    else{
      return (
        <div id="CowBattDayWrapper">
          {battDay.response.map(item => ( 
            <div key={item.id}>
              <h3 className="CowBatDayHeader">Batt Volt - mV</h3>
              <BarChart width={450} height={270} data={[
                          {'name': '00_00', 'reading': item.BATT_00_00,},
                          {'name': '04_00', 'reading': item.BATT_04_00,},
                          {'name': '08_00', 'reading': item.BATT_08_00,},
                          {'name': '12_00', 'reading': item.BATT_12_00,},
                          {'name': '16_00', 'reading': item.BATT_16_00,},
                          {'name': '20_00', 'reading': item.BATT_20_00,},
                        ]}
                        margin={{top: 40, right: 20, left: 10, bottom: 0}}>
              < CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="name"/>
                <YAxis/>
                <Tooltip/>
                <Legend />
                <Bar dataKey="reading" fill="#228B22" />
              </BarChart> 
            </div> 
            ))}
        </div>  
      );
    }
  }
}
export default CowBattDay;


