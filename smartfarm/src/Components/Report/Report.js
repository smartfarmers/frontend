import React, { useState, Fragment, Component } from 'react'
import AddReportForm from './forms/AddReportForm'
import './Report.css'
import { Redirect } from 'react-router-dom';

class Report extends Component {
	constructor(props) {
		super(props);
	}

render() {
	if (!localStorage.getItem('token')) {
		return (
			<Redirect to={{ pathname: '/' }} push/>
		) 
	}
	return (
	  <div className="report-container">
		  <h1>Reports</h1>
		<div className="report-flex-row">
		  <div className="report-flex-large">
			  <Fragment>
				<h2>Generate CSV Report</h2>
				<AddReportForm />
			  </Fragment>
		  </div>
		</div>
	  </div>
	) 
  }
}
export default Report;




