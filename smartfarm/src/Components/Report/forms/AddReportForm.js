import React, { Component } from 'react'
import { readRemoteFile } from 'react-papaparse'

class AddReportForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            items: [], 
            value: null, 
            start_date: null,
            end_date: null,  
        };
        this.handleChange = this.handleChange.bind(this);
    }

    // fetches all the cow id's for drop down lis
    componentDidMount(){
       fetch(`${process.env.REACT_APP_API_URL}/cowId`, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
            .then(res => res.json())
            .then(json => {
                this.setState({
                    isLoaded: true,
                    items: json, 
                }) 
        });
    }
 
    // handles change for text fields
    changeHandler = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    // handles change for select option fields
    handleChange(event) {
        this.setState({value: event.target.value});
    }

    // activityDailyreport
    handleActivityDailyClick() {
        if (!this.state.value || !this.state.start_date || !this.state.end_date){
            window.alert('Please enter a value in all the fields')
        }
        
        fetch(`${process.env.REACT_APP_API_URL}/activityDailyReport/` + this.state.value + '/' + this.state.start_date + '/' + this.state.end_date,
        {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
            .then(resc => resc.json())
            .then(json => {
                if(json.status == 500 ){
                    window.alert("No records between these dates - please reselect start and end dates")
                }
                if(json.status == 200 ){
                    window.alert("Generating report .... click Download Report")
                }
        });
    }

    // activity4secsReport
    handleActivityFourSecondsClick() {
        if (!this.state.value || !this.state.start_date || !this.state.end_date){
            window.alert('Please enter a value in all the fields')
        }  

        fetch(`${process.env.REACT_APP_API_URL}/activity4secsReport/` + this.state.value + '/' + this.state.start_date + '/' + this.state.end_date,
        {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
            .then(resc => resc.json())
            .then(json => {
                if(json.status == 500 ){
                    window.alert("No records between these dates - please reselect start and end dates")
                }
                if(json.status == 200 ){
                    window.alert("Generating report .... click Download Report")
                }
        });
    }
                
    // location5minsReport  
    handleLocationFiveMinsClick() {
        if (!this.state.value || !this.state.start_date || !this.state.end_date){
            window.alert('Please enter a value in all the fields')
        }  

        fetch(`${process.env.REACT_APP_API_URL}/location5minsReport/` + this.state.value + '/' + this.state.start_date + '/' + this.state.end_date,
        {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
            .then(resc => resc.json())
            .then(json => {
                if(json.status == 500 ){
                    window.alert("No records between these dates - please reselect start and end dates")
                }
                if(json.status == 200 ){
                    window.alert("Generating report .... click Download Report")
                }
        });
    }
            
    // cowTemperatureReport 
    handleTempClick() { 
        if (!this.state.value || !this.state.start_date || !this.state.end_date){
            window.alert('Please enter a value in all the fields')
        } 

        fetch(`${process.env.REACT_APP_API_URL}/cowTemperatureReport/` + this.state.value + '/' + this.state.start_date + '/' + this.state.end_date,
        {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
            .then(resc => resc.json())
            .then(json => {
                if(json.status == 500 ){
                    window.alert("No records between these dates - please reselect start and end dates")
                }
                if(json.status == 200 ){
                    window.alert("Generating report .... click Download Report")
                }
        });
    }       
    
    // cowWeightReport 
    handleCowWeightClick() { 
        if (!this.state.value || !this.state.start_date || !this.state.end_date){
            window.alert('Please enter a value in all the fields')
        } 

        fetch(`${process.env.REACT_APP_API_URL}/cowWeightReport/` + this.state.value + '/' + this.state.start_date + '/' + this.state.end_date,
        {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
            .then(resc => resc.json())
            .then(json => {
                if(json.status == 500 ){
                    window.alert("No records between these dates - please reselect start and end dates")
                }
                if(json.status == 200 ){
                    window.alert("Generating report .... click Download Report")
                }
        });
    }   
    
    // download csv
    handleDownloadClick(reportType) {
        let url = ""
        let fileName = "" 
        switch (reportType) {
            case "daily":
                url = `${process.env.REACT_APP_API_URL}/ActDailyReport`
                fileName = "ActDailyReport.csv"
                break
            case "act":
                url = `${process.env.REACT_APP_API_URL}/ActFourSecondsReport`
                fileName = "ActFourSecondsReport.csv"
                break
            case "loc":
                url = `${process.env.REACT_APP_API_URL}/LocationFiveReport`
                fileName = "LocationReport.csv"
                break
            case "temp":
                url = `${process.env.REACT_APP_API_URL}/TempReport`
                fileName = "TemperatureReport.csv"
                break
            case "weight":
                url = `${process.env.REACT_APP_API_URL}/WeightReport`
                fileName = "WeightReport.csv"
                break
        }
        fetch(url)
            .then(response => response.body)
            .then(body => {
                const reader = body.getReader();
                return new ReadableStream({
                    start(controller) {
                      return pump();
                      function pump() {
                        return reader.read().then(({ done, value }) => {
                          // When no more data needs to be consumed, close the stream
                          if (done) {
                              controller.close();
                              return;
                          }
                          // Enqueue the next data chunk into our target stream
                          controller.enqueue(value);
                          return pump();
                        });
                      }
                    }  
                  })
                })
                .then(stream => new Response(stream))
                .then(response => response.blob())
                .then((blob) => {
                    // 2. Create blob link to download
                    const url = window.URL.createObjectURL(new Blob([blob]));
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', fileName);
                    // 3. Append to html page
                    document.body.appendChild(link);
                    // 4. Force download
                    link.click();
                    // 5. Clean up and remove the link
                    link.parentNode.removeChild(link);
                    this.setState({
                        loading: false
                    });
                })
                .catch((error) => {
                    error.json().then((json) => {
                        this.setState({
                            errors: json,
                            loading: false
                        });
                    })
                }); 
    }

    renderCowIdList(){
        var {isLoaded} = this.state;
        if (!isLoaded) {
            return <div>Loading...cowID List</div>;
        }
        else{	
            return (
                <select className="ReportSelector" value={this.state.value} onChange={this.handleChange}>
                <option value="" disabled selected>Select Cow ID</option>
                    {this.state.items.response.map((obj) => 
                        <option key={obj.cow_id} value={obj.cow_id}> Cow ID: {obj.cow_id} </option>) }
            </select>
            )
        }
    }
            
    render() {
        const { start_date, end_date} = this.state
        return (
            <div>
                <div className="ReportSelectors">
                    <h6>Cow ID</h6>
                    {this.renderCowIdList()}
                    <h6>Start date</h6>	
                        <input type = "date" className="ReportSelector" name="start_date" value={start_date} onChange={this.changeHandler}/>
                    <h6>End date</h6>	
                        <input type = "date" className="ReportSelector" name="end_date" value={end_date} onChange={this.changeHandler}/>
                </div>&nbsp; 
                <div className="ReportButtonContainer">  
                    <label className="ReportButtonHeader"><h6>Activity Daily:</h6></label>
                    <button type="report-submit" onClick={() => {
                        this.handleActivityDailyClick()}}>
                        Generate CSV
                    </button>
                    <button type="report-submit" className="ReportDownloadButton" onClick={() => {
                        this.handleDownloadClick("daily")}}>
                        Download Report
                    </button> 
                </div>&nbsp; 
                <div className="ReportButtonContainer"> 
                    <label className="ReportButtonHeader"><h6>Activity 4 seconds:</h6></label>
                    <button type="report-submit" onClick={() => {
                        this.handleActivityFourSecondsClick()}}>
                        Generate CSV
                    </button>
                    <button type="report-submit" className="ReportDownloadButton" onClick={() => {
                        this.handleDownloadClick("act")}}>
                        Download Report
                    </button>       
                </div>&nbsp; 
                <div className="ReportButtonContainer"> 
                    <label className="ReportButtonHeader"><h6>Location 5 minutes:</h6></label>
                    <button type="report-submit" onClick={() => {
                        this.handleLocationFiveMinsClick()}}>
                        Generate CSV
                    </button>
                    <button type="report-submit" className="ReportDownloadButton" onClick={() => {
                        this.handleDownloadClick("loc")}}>
                        Download Report
                    </button>
                </div>&nbsp;   
                <div className="ReportButtonContainer">
                    <label className="ReportButtonHeader"><h6>Temperature:</h6></label>
                    <button type="report-submit" onClick={() => {
                        this.handleTempClick()}}>
                        Generate CSV
                    </button>
                    <button type="report-submit" className="ReportDownloadButton" onClick={() => {
                        this.handleDownloadClick("temp")}}>
                        Download Report
                    </button>
                </div>&nbsp; 
                <div className="ReportButtonContainer"> 
                    <label className="ReportButtonHeader"><h6>Weight:</h6></label>
                    <button type="report-submit" onClick={() => {
                        this.handleCowWeightClick()}}>
                        Generate CSV
                    </button>
                    <button type="report-submit" className="ReportDownloadButton" onClick={() => {
                        this.handleDownloadClick("weight")}}>
                        Download Report
                    </button>
                </div>
            </div>
        );
    }
}
export default AddReportForm