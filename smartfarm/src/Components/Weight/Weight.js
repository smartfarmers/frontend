import React, { useState, Fragment, Component } from 'react'
import AddWeightForm from './forms/AddWeightForm'
import EditWeightForm from './forms/EditWeightForm'
import WeightTable from './tables/WeightTable'
import Error from '../Errors/Error'
import { Redirect } from 'react-router-dom';

// Setting state
class Weight extends Component {
	constructor(props) {
		super(props);
		this.state = {
		  cowData: [],
		  cowGetResponse: [],
		  cowPostResponse: null,
		  currentCow: null,
		  editing: null,
		  setEditing: '',
		  getError: null,
      	  postError: null,
      	  putError: null,
		  deleteError: null,
		  putError: null
		}
		this.deleteCow = this.deleteCow.bind(this)
		this.editRow = this.editRow.bind(this)
		this.updateCow = this.updateCow.bind(this)
		this.mapGetCowResponse = this.mapGetCowResponse.bind(this)
		this.mapNewCow = this.mapNewCow.bind(this)
		this.getCowList = this.getCowList.bind(this)
		this.mapUpdateCow = this.mapUpdateCow.bind(this)
	  }

	componentDidMount(){
		this.getCowList()
	}

	  // read weight entries
	  getCowList(){
		fetch(`${process.env.REACT_APP_API_URL}/weight/`, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
       	.then(res => res.json())
       	.then(json => {
			if(json.status != 200){
				this.setState({
				  cowGetResponse: json,
				  getError: json.status
				})
			  } else {
				let mappedResponse = this.mapGetCowResponse(json.response)
					this.setState({
						cowGetResponse: json,
						cowData: mappedResponse
			  		})
			  	}
		  }).catch(error => {
			this.setState({
			  getError: error
			})
		  });
	}

	mapGetCowResponse(response) {
		let result = []
		var id = 0
		response.forEach(element => {
			let item = {
				id: id,
				weight_id: element["WEIGHT_ID"],
				cow_id: element["COW_ID"],
				weigh_date: element["WEIGH_DATE"],
				weighed_by: element["WEIGHED_BY"],
				weight_kg: element["WEIGHT_KG"],
			}
			result.push(item)
			id++
		});
		return result
	}

	mapNewCow(cow) {
    	let item = {
			weight_id: cow["weight_id"],
			cow_id: cow["cow_id"],
			weighed_by: cow["weighed_by"],
			weight_kg: cow["weight_kg"],
			weigh_date: cow["weigh_date"]
    	}
    	return item
	}

	mapUpdateCow(cow) {
    	let item = {
			weight_id: cow["weight_id"],
			cow_id: cow["cow_id"],
			weighed_by: cow["weighed_by"],
			weight_kg: cow["weight_kg"],
			weigh_date: cow["weigh_date"]
    	}
    	return item
	}

	// delete existing weight entry
	deleteCow(cow) {
    	fetch(`${process.env.REACT_APP_API_URL}/weight/` + cow["weight_id"], {
		  method: 'delete',
		  headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}
    	}).then(res => res.json())
      	  .then(json => {
        if(json.status != 200 ){
          this.setState({
            deleteError: json
          })
        }
        this.setState({
            cowDeleteResponse: json,
      }, () => {
        this.getCowList()
      })
    }).catch(error => {
      this.setState({
        deleteError: error
      })
    });
	}

//update existing weight entries
updateCow(id, updatedCow) {  
	this.setState({
	  editing: false,
	  cowData: this.state.cowData.map(cow => (cow.id === id ? updatedCow : cow))
	})

	let mappedCow = this.mapUpdateCow(updatedCow);
	fetch(`${process.env.REACT_APP_API_URL}/weight/` + updatedCow.weight_id, {	
      method: 'put',
      headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
      body: JSON.stringify(mappedCow)
    }).then(res => res.json())
      .then(json => {
		if(json.status != 200 ){
			this.setState({
				putError: json
			})
		}
        this.setState({
            cowPutResponse: json,
        }, () => {
        this.getCowList()
      })
    }).catch(error => {
      this.setState({
        putError: error
      })
    });
}
	
// edit
editRow(cow) {
	this.setState({
	  editing: true,
      currentCow: { weight_id: cow.weight_id, cow_id: cow.cow_id, weighed_by: cow.weighed_by, weight_kg: cow.weight_kg, weigh_date: cow.weigh_date }
	})
}

getError(){
    if(this.state.getError){
      return (
        <Error
          errorType="error"
          errorHeading="Unable to retrieve list of weights" 
          errorMessage="We were unable to obtain weights list, please contact application administrator">
        </Error>
      )
    }
}

deleteError(){
    if (this.state.deleteError) {
      return (
        <Error
          errorType="error"
          errorHeading="Unable to delete weight entry" 
          errorMessage="We were unable to delete the weight entry, please contact application administrator">
        </Error>
      )
    }
}

putError(){
    if(this.state.putError){
      return (
        <Error
          errorType="error"
          errorHeading="Unable to update weight" 
          errorMessage="We were unable to update a weight entry, please contact application administrator">
        </Error>
      )
	}
}  

render() {
	if (!localStorage.getItem('token')) {
		return (
			<Redirect to={{ pathname: '/' }} push/>
		) 
	}
	return (
	  <div className="weight-container">
		<h1 className="WeightHeading">Weight Management</h1>
		<div className="weight-flex-row">
		  <div className="weight-flex-large">
			{this.state.editing ? (
			  <Fragment>
				<h2>Edit Weight</h2>
				<EditWeightForm
				  editing={this.state.editing}
				  setEditing={this.state.setEditing}
				  currentCow={this.state.currentCow}
				  updateCow={this.updateCow}
				/>
			  </Fragment>
			) : (
			  <Fragment>
				<h2>Add Weight</h2>
				<AddWeightForm addCow={this.addCow} />
			  </Fragment>
			)}
		  </div>
		  <div className="weight-flex-large">
			<h2>View Weight</h2>
			<WeightTable cows={this.state.cowData} editRow={this.editRow} deleteCow={this.deleteCow} />
		  </div>
		</div>
			<div className="flex-row-weight-errors">
          		{ this.getError() } 
				{ this.deleteError() }  
				{ this.putError() }
        	</div>	
	  </div>
	) 
  }
}

export default Weight;




