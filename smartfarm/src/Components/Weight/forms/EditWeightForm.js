import React, { useState, useEffect } from 'react'

  const EditWeightForm = props => {
  const [ cow, setCow ] = useState(props.currentCow)
  const initialFormState = { cow_id: '', weighed_by: '', weight_kg: '', weigh_date: ''}

  useEffect(() => {
     setCow(props.currentCow)},
    [ props ]
  )

  const handleInputChange = event => {
  const { name, value } = event.target

  setCow({ ...cow, [name]: value })}

  return (
    <form
        onSubmit={event => {
          if (!cow.weighed_by || !cow.weight_kg || !cow.weigh_date){
            window.alert('Please enter a value in all the fields')
          }
          event.preventDefault()
          props.updateCow(cow.weight_id, cow)
        }}
    >
        <h6>Cow ID</h6>
		    <input type="text" name="cow_id" value={cow.cow_id} onChange={handleInputChange} disabled = {true}/>

		    <h6>Weighed By - Name</h6>
		    <input type="text" name="weighed_by" value={cow.weighed_by} pattern="[A-Za-z]+" onChange={handleInputChange} />

		    <h6>Weight - KG</h6>
		    <input type="text" name="weight_kg" value={cow.weight_kg} pattern="[0-9]+" onChange={handleInputChange} />

		    <h6>Weigh date</h6>
		    <input type="date" name="weigh_date" value={cow.weigh_date} onChange={handleInputChange} />
    
        <button type="weight-submit">Update</button>&nbsp;

        <button  type="weight-submit" onClick={() => { 
          if (window.confirm('Are you sure you wish to cancel this update?')) setCow(initialFormState)} } >Cancel</button>
    </form>
  )
}

export default EditWeightForm






