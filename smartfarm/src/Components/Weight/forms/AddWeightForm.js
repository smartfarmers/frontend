import React, { Component } from 'react'
import './Weight.css'
import Error from '../../Errors/Error'

class AddWeightForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            isError: false,
            isPostError: false,
            items: [], // for list of ALL cow-id's from DB
            value: '', // for option list - cowId's
            weighed_by: '',
            weight_kg: '',
            weigh_date: '',
            cowGetResponse: [],
            cowPostResponse: null,
            cowData:[],
            getError: null,
            postError: false
        };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    }

    // fetches all the cow id's for drop down list
    componentDidMount(){
      fetch(`${process.env.REACT_APP_API_URL}/cowId`, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
        .then(res => res.json())
        .then(json => {
          if(json.status != 200){
            this.setState({
              items: json,
              isError: true,
              getError: json.status
            })
          } else {
            this.setState({
              isLoaded: true,
              items: json,
          })
          }
      }).catch(error => {
        this.setState({
          getError: error,
          isError: true,
        })
      });
    }

      getError(){
        if(this.state.getError){
          return (
            <Error
              errorType="error"
              errorHeading="Unable to retrieve list of cows" 
              errorMessage="Please contact application administrator">
            </Error>
          )
        }
      }

      postError (){
        if(this.state.postError){
          return (
            <Error
              errorType="error"
              errorHeading="Unable to add Weight" 
              errorMessage="Contact application administrator">
            </Error>
          )
       } 
     }
     
    // handles change for text fields
    changeHandler = e => {
        this.setState({ [e.target.name]: e.target.value })
    }
	
    // handles change for select option fields
    handleChange(event) {
        this.setState({value: event.target.value});
    }
  
    // handles submit event
    handleSubmit(event) {	
        if (!this.state.value || !this.state.weighed_by || !this.state.weight_kg || !this.state.weigh_date){
             window.alert('Please enter a value in all the fields')
        }
        let weightInfo={
            cow_id:this.state.value,
            weighed_by:this.state.weighed_by,
            weight_kg:this.state.weight_kg,
            weigh_date:this.state.weigh_date
        };
    
        
    // POST weight details to DB
    fetch(`${process.env.REACT_APP_API_URL}/weight`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
        body: JSON.stringify(weightInfo)
        }).then(res => res.json())
          .then(json => {
            if(json.status != 201 ){
              this.setState({
              postError:true,
              isPostError: true
            })
          }
        }).catch(error => {
            this.setState({
              postError: error,
              isPostError: true
            })
        });
    }

   
    // Get's cow id's from DB - put into dropdown
    render() {

        var curr = new Date();
        curr.setDate(curr.getDate());
        var date = curr.toISOString().substr(0,10);
        
        const { weighed_by, weight_kg, weigh_date } = this.state
        var {isLoaded, isError} = this.state;

       if (isError) {
          return <div className="flex-row-cow-errors">
            {this.getError()} </div>
        }
        else if(!isLoaded) {
          return <div>Loading...cowID List</div>;
        }
        else{	
          return (
                <form onSubmit={this.handleSubmit}>
                    <h6>Cow ID</h6>
                    <select type="weight-select" value={this.state.value} onChange={this.handleChange}>
                            <option value="" disabled selected>Select Cow ID</option>
                                {this.state.items.response.map((obj) => 
                                    <option key={obj.cow_id} value={obj.cow_id}> Cow ID: {obj.cow_id} </option>) }
                    </select>
					          <div>
                    <h6>Weighed By</h6>
							          <input 
								            placeholder="Name"
								            type = "text" 
								            name = "weighed_by" 
								            value={weighed_by}
								            onChange={this.changeHandler}pattern="[A-Za-z]+"/>
					          </div>

                    <div>               
                    <h6>Weight in KG</h6>	
                        <input 
                            placeholder="538"
                            type = "text" 
                            name = "weight_kg" 
                            value={weight_kg}
                            onChange={this.changeHandler}pattern="[0-9]+"/>
                    </div>   

                    <div >          
						        <h6>Weigh date</h6>	
                            <input type = "date" name = "weigh_date" value={weigh_date} onChange={this.changeHandler}/>
                            <button type="weight-submit">Submit </button>
                    </div> 
                    <div className="flex-row-post-weight-errors">
          		              {this.postError()} 
                    </div>         
                  </form>
            );
        }
    }
}
export default AddWeightForm

