import React from 'react'	

const WeightTable = props => (
  <table className = "weightTable">
    <thead className = "weightHead">
      <tr className = "weightTr">
        <th className = "weightTh"><h6>Cow ID</h6></th>
        <th className = "weightTh"><h6>Weighed By</h6></th>
        <th className = "weightTh"><h6>Weight kg</h6></th>
        <th className = "weightTh"><h6>Weigh Date</h6></th>
      </tr>
    </thead>
    <tbody>
      {props.cows.length > 0 ? (
        props.cows.map(cow => (
          <tr className = "weightTr" key={cow.id}>
            <td className = "weightTd">{cow.cow_id}</td>
            <td className = "weightTd">{cow.weighed_by}</td>
            <td className = "weightTd">{cow.weight_kg}</td>
            <td className = "weightTd">{cow.weigh_date}</td>  
            <td className = "weightTd">
                <button type="weight-submit" onClick={() => { 
                    props.editRow(cow)}}>Edit </button>&nbsp;
                <button type="weight-submit"onClick={() => { 
                    if (window.confirm('Are you sure you wish to delete this weight entry?')) props.deleteCow(cow)}}type="weight-submit">Delete </button>
            </td>
          </tr>
        ))
      ) : (
        <tr className = "weightTr">
          <td className = "weightTd" colSpan={3}>No weight entries</td>
        </tr>
      )}
    </tbody>
  </table>
)

export default WeightTable









