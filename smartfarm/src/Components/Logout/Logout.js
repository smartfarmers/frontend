import React, { Fragment, Component } from 'react';
import { Redirect } from 'react-router-dom'; 
import './Logout.css';

class Logout extends Component {

    constructor(props){
        super(props);
        this.state = {
            login: false
        }
        this.handleClick = this.handleClick.bind(this);        
      }

    handleClick() {
        this.setState({
            login: true
        })
    }

    render () {
        if (this.state.login === true) {
            return <Redirect to={{ pathname: '/' }} push/>
        }
        return (
            localStorage.clear(),
            <div className="container">
            <Fragment>
                <h1>Successfully logged out</h1>
                <button type="login" class={"LoginButton"} onClick={this.handleClick}>Login</button>
            </Fragment>
            </div>            
        )                   
    }    
}

export default Logout;