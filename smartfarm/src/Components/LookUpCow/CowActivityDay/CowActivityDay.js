
import React from "react";
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';
import './CowActivityDay.css';

class CowActivityDay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      actData: [],
      isLoaded: false,
      cowId: '',
      date:''
    }
  }

  // fetches temperature with cow_id for a specified date
  componentDidMount(){
    this.setState({
      cowId: this.props.cowId,
      date: this.props.date
    })
    fetch(`${process.env.REACT_APP_API_URL}/actday/` + this.props.cowId + '/' + this.props.date, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
      .then(res => res.json())
      .then(json => {
       this.setState({
         isLoaded: true,
         actData: json, 
       }) 
    }); 
  }

  parseActData(response){
    let result = []
    response.forEach(element => {

        let resting;
        let grazing;
        let walking;
       
        try{
            resting = element["hourSummaries"][0]["count"] // act_stat: 1 = Resting
        } catch{
            resting = 0;
        }
        try{
            grazing = element["hourSummaries"][1]["count"] // act_stat: 2 = Grazing
        } catch{
            grazing = 0;
        }
        try{
            walking = element["hourSummaries"][2]["count"] // act_stat: 3 = Walking
        } catch{
            walking = 0;
        }
       
        let dateTime = {
            "dateTime" : `${element["day"]} - ${element["hour"]}`,
            "resting" : resting,
            "grazing" : grazing,
            "walking" : walking
        }
        result.push(dateTime)
    });
    return result;
  }

  
  render() {
    var {isLoaded, actData } = this.state;
    if (!isLoaded) {
      return <div className="LookUpCowLoading">Loading...</div>;
    } else if(actData.response == null){
      return (
        <div className="LookUpCowNoData">No Activity data for this date</div>
      );

    } else{
        let parsedResponse = this.parseActData(actData.response)
      return (
        <div id="CowActivityDayWrapper">
            <h3>Cow Activity</h3>
            <ResponsiveContainer width="100%" height={300}>
            <LineChart data={parsedResponse}
                    margin={{top: 2, right: 30, left: 5, bottom: 20}}>
            < CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="dateTime"/>
                <YAxis/>
                <Tooltip/>
                <Legend />
                <Line dataKey="resting" stroke="#1E90FF" />
                <Line dataKey="grazing" stroke="#228B22" />
                <Line dataKey="walking" stroke="#00008b" />
                
            </LineChart> 
            </ResponsiveContainer>
            </div>
      );
    }
  }
}
export default CowActivityDay;
