import React, { Component } from 'react';
import CowTempRange from '../../CowTempRange/CowTempRange';
import CowBattRange from '../../CowBattRange/CowBattRange';
import CowDetails from '../../CowDetails/CowDetails';
import CowActivityRange from '../CowActivityRange/CowActivityRange';
import CowMapRange from '../CowMapRange/CowMapRange';
import './CowRangeDisplay.css';

class CowRangeDisplay extends Component {
    constructor(props){
        super(props)
        this.state = {
            cowId: null,
            startDate: null,
            endDate: null
        }
    }

    componentDidMount(){
        const { cowId } = this.props.match.params
        const { startDate } = this.props.location.state
        const { endDate } = this.props.location.state
        this.setState({
          cowId: cowId,
          startDate: startDate,
          endDate: endDate
        })
    }

    render() {
        if(this.state.cowId){
            return (
                <div className="LookUpCowRangeDisplay">
                    <h1 className="LookUpCowRangeSearchTitle">Look Up Cow - Date Range</h1>
                    <CowDetails items={[this.state.cowId]} />
                    <CowActivityRange cowId={this.state.cowId} startDate={this.state.startDate} endDate={this.state.endDate} />
                    <div className="LookUpCowRangeDisplayGraphWrapper">
                        <CowBattRange cowId={this.state.cowId} startDate={this.state.startDate} endDate={this.state.endDate} />
                        <CowTempRange cowId={this.state.cowId} startDate={this.state.startDate} endDate={this.state.endDate} />
                    </div>
                    <CowMapRange cowId={this.state.cowId} startDate={this.state.startDate} endDate={this.state.endDate} />
                </div>
            )
        } else {
            return (
                <div className="LookUpCowRangeDisplay">
                    <h1 className="SearchTitle">Loading...</h1>
                </div>
            )
        }
    }
}

export default CowRangeDisplay