import React, { Component } from 'react';
import './LookUpCow.css';
import CowSelectionDetails from './CowSelectionDetails/CowSelectionDetails';
import { Redirect } from 'react-router-dom'; 

class LookUpCow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            cowId: '',
        }
      }

    render() {
        if (!localStorage.getItem('token')) {
            return (
                <Redirect to={{ pathname: '/' }} push/>
            ) 
        }
        return (
            <div className="LookUpCow">
                <div className="LookUpCowCard">
                    <h1 className="SearchTitle">Look Up Cow</h1>
                    <CowSelectionDetails />
                </div>
            </div>
        )
    }
}

export default LookUpCow