import React, { Component } from 'react';
import { ReactBingmaps } from 'react-bingmaps';
import './CowMapRange.css';

class CowMapRange extends Component {
    constructor(props) {
        super(props);
        this.state = {
          locData: [],
          isLoaded: false,
          cowId: null,
          startDate: null,
          endDate: null
        }
      }

      handlePinOnHover(event){}

    // fetches location with cow_id for a specified date
    componentDidMount(){
        let cowId  = this.props.cowId
        let startDate = this.props.startDate
        let endDate = this.props.endDate
    
        this.setState({
          cowId: cowId,
          startDate: startDate,
          endDate: endDate
        })
    
        this.getLocRangeData(cowId, startDate, endDate);
    }

    async getLocRangeData(cowId, startDate, endDate){
        let daysDiff = this.getDifferenceInDates(startDate, endDate)
        let results = []
        for(let i = 0; i < daysDiff; i++){
          let day = new Date();
          let addValue = 1;
          if( i == 0 ){
            addValue = 0
          }
          day = startDate.setDate(startDate.getDate() + addValue);
          day = new Date(day);
          day = this.formatDate(day)
          let res = await this.getLocData(day, cowId);
          if(res.response){
            results = results.concat(res.response)
          }
        }
        this.setState({
          isLoaded: true,
          locData: results, 
        }) 
      }

    async getLocData(date, cowId){
        let res = await fetch(`${process.env.REACT_APP_API_URL}/locday/` + cowId + '/' + date, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
          .then(res => res.json())
          .then(json => {
            return json
          });
        return await res
      }

      getDifferenceInDates(startDate, endDate){
        let diff = endDate.getTime() - startDate.getTime();
        let diffInDays = diff / (1000 * 3600 * 24);
        return parseInt(diffInDays)
      }
    
      formatDate(date){
        let formattedDate = date.toISOString()
        formattedDate = formattedDate.split("T")[0]
        return formattedDate
      }

      parseCowLocData(response){
        // Make the mouse over event for the pins display the date time of the pin and cow id
        let result = []
        response.forEach(element => {
            if(element["loc_lat"] != null || element["loc_lon"] != null){
                let pin = {
                    "location" : [
                        element["loc_lat"],
                        element["loc_lon"]
                    ],
                    "option" : {
                        "color" : "red"
                    },
                    "addHandler" : {
                        "type" : "mouseover",
                        callback : this.handlePinOnHover()
                    }
                };
                result.push(pin);
            }
        });
        return result;
    }

    render() {
      var {isLoaded, locData } = this.state;
      if (!isLoaded) {
        return <div className="LookUpCowLoading">Loading...</div>;
      } else if(locData.length === 0){
      return <div className="LookUpCowNoData">No Location data for this date range</div>
      } else {
        let parsedLocData = this.parseCowLocData(locData)
        return (
            <div className="CowMapRange">
                <h3 className="CowMapHeading">CowMap</h3>
                <ReactBingmaps 
                    bingmapKey = "Al4i0oHFLW-z-hDROXL89Wp37vGOgqMJT6aX6ompovuUH9Ef54yzbBuGdyWhRtEe"
                    center = {[-35.0479, 147.312]}
                    mapTypeId= {"aerial"}
                    navigationBarMode={"minified"}
                    pushPins={parsedLocData}
                />
            </div>
        )
      }
  }
}

export default CowMapRange