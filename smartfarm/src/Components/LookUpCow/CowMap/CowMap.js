import React, { Component } from 'react';
import { ReactBingmaps } from 'react-bingmaps';
import './CowMap.css';

class CowMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
          locData: [],
          isLoaded: false,
          cowId: '',
          date:''
        }
      }

      handlePinOnHover(event){
          console.log("mouse Over: " + event)
      }

    // fetches location for day with cow_id for a specified date
    componentDidMount(){
        this.setState({
            cowId: this.props.cowId,
            date: this.props.date
          })
        fetch(`${process.env.REACT_APP_API_URL}/locday/` + this.props.cowId + '/' + this.props.date, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
            .then(res => res.json())
            .then(json => {
                this.setState({
                    isLoaded: true,
                    locData: this.parseCowLocData(json.response), 
            }) 
        });
    }

    parseCowLocData(response){
        // Make the mouse over event for the pins display the date time of the pin and cow id
        let result = []
        response.forEach(element => {
            if(element["loc_lat"] != null || element["loc_lon"] != null){
							let pin = {};
                try {
                    pin = {
                        "location" : [
                            element["loc_lat"],
                            element["loc_lon"]
                        ],
                        "option" : {
                            "color" : "red"
                        },
                        "addHandler" : {
                            "type" : "mouseover",
                            callback : this.handlePinOnHover()
                        }
                    };
                } catch {}
                result.push(pin);
            }
        });
        return result;
    }

    render() {
        var {isLoaded, locData } = this.state;
        if (!isLoaded) {
          return <div className="LookUpCowLoading">Loading...</div>;
        } else if(locData.length === 0){
            return <div className="LookUpCowNoData">No Location data for this date</div>
        } else {
            return (
                <div className="CowMap">
                    <h3 className="CowMapHeading">Cow Map</h3>
                    <ReactBingmaps 
                        bingmapKey = "Al4i0oHFLW-z-hDROXL89Wp37vGOgqMJT6aX6ompovuUH9Ef54yzbBuGdyWhRtEe"
                        center = {[-35.0479, 147.312]}
                        mapTypeId= {"aerial"}
                        navigationBarMode={"minified"}
                        pushPins={locData}
                    />
                </div>
            )
        }
    }
}

export default CowMap