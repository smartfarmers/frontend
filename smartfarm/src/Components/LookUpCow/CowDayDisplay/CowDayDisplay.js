import React, { Component } from 'react';
import CowTempDay from '../../CowTempDay/CowTempDay';
import CowBattDay from '../../CowBattDay/CowBattDay';
import CowDetails from '../../CowDetails/CowDetails';
import CowActivityDay from '../CowActivityDay/CowActivityDay';
import CowMap from '../CowMap/CowMap';
import './CowDayDisplay.css';

class CowDayDisplay extends Component {
    constructor(props){
        super(props)
        this.state = {
            cowId: null,
            startDate: null
          }
    }

    componentDidMount(){
        const { cowId } = this.props.match.params
        const { startDate } = this.props.location.state
        this.setState({
            cowId: cowId,
            startDate: this.formatDate(startDate)
            
        })
    }

    formatDate(date){
        let formattedDate = date.toISOString()
        formattedDate = formattedDate.split("T")[0]
        return formattedDate
      }

    render() {
        if(this.state.cowId && this.state.startDate){
            
            return (
                <div className="LookUpCowDayDisplay">
                    <h1 className="LookUpCowDaySearchTitle">Look Up Cow</h1>
                    <CowDetails items={[this.state.cowId]} />
                    <CowActivityDay cowId={this.state.cowId} date={this.state.startDate} /> 
                    <div className="LookUpCowDayDisplayGraphWrapper">
                        <CowBattDay cowId={this.state.cowId} date={this.state.startDate} />
                        <CowTempDay cowId={this.state.cowId} date={this.state.startDate} />
                        <CowMap cowId={this.state.cowId} date={this.state.startDate} /> 
                    </div>
                </div>
            )
        } else {
            return (
                <div className="LookUpCowDayDisplay">
                    <h1 className="SearchTitle">Loading...</h1>
                </div>
            )
        }
    }
}

export default CowDayDisplay