import React, { Component, Form } from "react";
import "./CowSelectionDetails.css";
import RangeDateDetails from "../DateDetails/RangeDateDetails";
import SingleDateDetails from "../DateDetails/SingleDateDetails";

class CowSelectionDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoaded: false,
        cowId: '',
        singleDay: false,
        range: false
    }
    this.handleTick = this.handleTick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.renderDatePicker = this.renderDatePicker.bind(this);
  }

  componentDidMount(){
    fetch(`${process.env.REACT_APP_API_URL}/cowlist`, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
      .then(res => res.json())
      .then(json => {
       this.setState({
         isLoaded: true,
         cowList: json.response, 
       })
    }); 
  }

  handleTick(event){
    if(event.target.name === "singleDay"){
      this.setState({
        range: false,
        singleDay: !this.state.singleDay
      }, () => { 
        this.forceUpdate()
      })
    } else if(event.target.name === "range"){
      this.setState({
        singleDay: false,
        range: !this.state.range
      }, () => { 
        this.forceUpdate()
      })
    }
  }

  handleChange(event){
    this.setState({
      cowId: event.target.value
    })
  }

  renderDatePicker() {
    if(this.state.cowId === ""){
      return(<div></div>)
    } else {
      if(this.state.singleDay === true){
        return(<SingleDateDetails cowId={this.state.cowId}/>)
      } else if (this.state.range === true) {
        return(<RangeDateDetails cowId={this.state.cowId}/>)
      }
    }
  }

  renderSelectionOrder() {
    if(this.state.cowId === ""){
      return(<span>Please select a CowID first</span>)
    } else { 
      return(<div></div>)
    }
  }

  render() {
    var {isLoaded } = this.state;
    if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="CowSelectionDetails">
          { this.renderSelectionOrder() }
          <form className="LookUpCowForm">
              <label className="cowIdLabel">Select Cow ID:</label>
              <select className="CowSelector" onChange={this.handleChange} >
                <option value="" disabled selected>Select your option</option>
                
                { this.state.cowList.map((cow) => <option key={cow.cow_id} value={cow.cow_id}>Cow ID: {cow.cow_id} Collar ID: {cow.collar_id}</option>) }
              </select>
              <div className="DateRangeSelector">
                  <label className="SelectRangeLabel">Select Range:</label>
                  <div className="DateTypeSelectors">
                    <label>
                        <input
                            name="singleDay"
                            type="checkbox"
                            className="SingleCheckBox"
                            checked={this.state.singleDay}
                            onChange={this.handleTick} />
                        Single Day
                </label>
                <label >
                    <input
                        name="range"
                        type="checkbox"
                        className="DateRangeCheckBox"
                        checked={this.state.range}
                        onChange={this.handleTick}
                        />
                    Date Range
                </label>
                </div>
              </div>
              { this.renderDatePicker() }
          </form>    
        </div>
      );
    }
  }
}

export default CowSelectionDetails;