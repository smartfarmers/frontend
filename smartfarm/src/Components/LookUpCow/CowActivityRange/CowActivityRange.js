
import React from "react";
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';
import './CowActivityRange.css';

class CowActivityRange extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      actData: [],
      isLoaded: false,
      cowId: null,
      startDate: null,
      endDate: null
    }
  }

  componentDidMount(){
    let cowId  = this.props.cowId
    let startDate = this.props.startDate
    let endDate = this.props.endDate

    this.setState({
      cowId: cowId,
      startDate: startDate,
      endDate: endDate
    })

    this.getActRangeData(cowId, startDate, endDate);
  }

  async getActRangeData(cowId, startDate, endDate){
    let daysDiff = this.getDifferenceInDates(startDate, endDate)
    let results = []
    for(let i = 0; i < daysDiff; i++){
      let day = new Date();
      let addValue = 1;
      if( i === 0 ){
        addValue = 0
      }
      day = startDate.setDate(startDate.getDate() + addValue);
      day = new Date(day);
      day = this.formatDate(day)
      let res = await this.getActData(day, cowId);
      if(res.response){
        results = results.concat(res.response)
      }
    }
    this.setState({
      isLoaded: true,
      actData: results, 
    }) 
  }

  getDifferenceInDates(startDate, endDate){
    let diff = endDate.getTime() - startDate.getTime();
    let diffInDays = diff / (1000 * 3600 * 24);
    return parseInt(diffInDays)
  }

  formatDate(date){
    let formattedDate = date.toISOString()
    formattedDate = formattedDate.split("T")[0]
    return formattedDate
  }


  async getActData(date, cowId){
    let res = await fetch(`${process.env.REACT_APP_API_URL}/actday/` + cowId + '/' + date, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
      .then(result => result.json())
      .then(json => {
        return json
      });
    return await res
  }

  parseActData(response){
    let result = []
    response.forEach(element => {

        let resting;
        let grazing;
        let walking;
        
        try{
            resting = element["hourSummaries"][0]["count"] // act_stat: 1 = Resting
        } catch{
            resting = 0;
        }
        try{
            grazing = element["hourSummaries"][1]["count"] // act_stat: 2 = Grazing
        } catch{
            grazing = 0;
        }
        try{
            walking = element["hourSummaries"][2]["count"] // act_stat: 3 = Walking
        } catch{
            walking = 0;
        }
       
        let dateTime = {
            "dateTime" : `${element["day"]} - ${element["hour"]}`,
            "resting" : resting,
            "grazing" : grazing,
            "walking" : walking
        }
        result.push(dateTime)
    });
    return result;
  }

  
  render() {
    var {isLoaded, actData } = this.state;
    if (!isLoaded) {
      return <div className="LookUpCowLoading">Loading...</div>;
    } else if(actData.length === 0){
      return <div className="LookUpCowNoData" >No Activity data for this date range</div>
    } else{
        let parsedResponse = this.parseActData(actData)
      return (
        <div id="CowActivityRange-wrapper">
            <h3>Cow Activity</h3>
            <ResponsiveContainer width="100%" height={300}>
            <LineChart data={parsedResponse}
                    margin={{top: 5, right: 30, left: 20, bottom: 5}}>
            < CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="dateTime"/>
                <YAxis/>
                <Tooltip/>
                <Legend />
                <Line dataKey="resting" stroke="#1E90FF" />
                <Line dataKey="grazing" stroke="#228B22" />
                <Line dataKey="walking" stroke="#00008b" />
            </LineChart> 
            </ResponsiveContainer>
            </div>
      );
    }
  }
}
export default CowActivityRange;
