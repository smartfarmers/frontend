import React, { Component } from "react";
import { Link } from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "./DateDetails.css";

class DateDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cowId : null,
      range : this.props.range,
      single : this.props.single,
      startDate : new Date(),
      endDate : new Date()
    };
  }

  componentDidMount(){
    this.setState({
      cowId: this.props.cowId,
      range: this.props.range,
      single: this.props.single
    })
  }

  handleChange = date => {    
    this.setState({
      startDate: date
    });
  };

  handleChangeEndDate = date => {    
    this.setState({
      endDate: date
    });
  };

  render() {
    if(this.state.range === "true"){
      return (
        <div className="DateDetails">
          <h2>Date Range Selector</h2>
          <div className="DateDetailsRange">
            <label className="DateDetailsSelectorLabel">Start Date:</label>
            <DatePicker
              className="StartDatePicker"
              selected={this.state.startDate}
              onChange={this.handleChange}
            />
            <label className="DateDetailsSelectorLabel">End Date:</label>
            <DatePicker
              className="endDatePicker"
              selected={this.state.endDate}
              onChange={this.handleChangeEndDate}
            />
            <Link 
              to={{
                pathname: `/CowRangeDisplay/${this.props.cowId}`,
                state: {
                  startDate: this.state.startDate,
                  endDate: this.state.endDate
                }
              }}
            >
              <button type="select-submit" className="DateDetailsRangeSubmitButton">Submit</button>
            </Link>
          </div>
        </div>
      );
    } else if (this.state.single === "true") {
      return (
        <div className="DateDetails">
          <h2>Single Date Selector</h2>
          <label className="DateDetailsSelectorLabel">Start Date:</label>
          <DatePicker
            className="StartDatePicker"
            selected={this.state.startDate}
            onChange={this.handleChange}
          />
          <Link 
            to={{
              pathname: `/CowDayDisplay/${this.props.cowId}`,
              state: {
                startDate: this.state.startDate
              }
            }}
          >
            <button type="select-submit">Submit</button>
          </Link>
        </div>
      );
    } else {
      return (<div></div>)
    }
  }
}

export default DateDetails;