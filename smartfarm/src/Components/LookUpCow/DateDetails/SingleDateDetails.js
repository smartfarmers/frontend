import React, { Component } from "react";
import { Link } from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "./DateDetails.css";

class SingleDateDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cowId : null,
      startDate : new Date(),
    };
  }

  componentDidMount(){
    this.setState({
      cowId: this.props.cowId,
    })
  }

  handleChange = date => {    
    this.setState({
      startDate: date
    });
  };

  render() {
      return (
        <div className="LookUpCowSingleDateDetails">
          <h2>Single Date Selector</h2>
          <label className="DateDetailsSelectorLabel">Start Date:</label>
          <DatePicker
            className="StartDatePicker"
            selected={this.state.startDate}
            onChange={this.handleChange}
          />
          <Link 
            to={{
              pathname: `/CowDayDisplay/${this.props.cowId}`,
              state: {
                startDate: this.state.startDate
              }
            }}
          >
            <button className="LookUpCowRangeSubmit">Submit</button>
          </Link>
        </div>
      );
  }
}

export default SingleDateDetails;