import React, { Component } from "react";
import { Link } from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "./DateDetails.css";

class RangeDateDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cowId : null,
      startDate : new Date(),
      endDate : new Date()
    };
  }

  componentDidMount(){
    this.setState({
      cowId: this.props.cowId,
    })
  }

  handleChange = date => {    
    this.setState({
      startDate: date
    });
  };

  handleChangeEndDate = date => {    
    this.setState({
      endDate: date
    });
  };

  render() {
    return (
    <div className="LookUpCowRangeDateDetails">
        <h2>Date Range Selector</h2>
        <div className="DateDetailsRange">
        <label className="DateDetailsSelectorLabel">Start Date:</label>
        <DatePicker
            className="StartDatePicker"
            selected={this.state.startDate}
            onChange={this.handleChange}
        />
        <label className="DateDetailsSelectorLabel">End Date:</label>
        <DatePicker
            className="endDatePicker"
            selected={this.state.endDate}
            onChange={this.handleChangeEndDate}
        />
        <Link 
            to={{
            pathname: `/CowRangeDisplay/${this.props.cowId}`,
            state: {
                startDate: this.state.startDate,
                endDate: this.state.endDate
            }
            }}
        >
            <button className="LookUpCowRangeSubmitButton">Submit</button>
        </Link>
        </div>
    </div>
    );
  }
}

export default RangeDateDetails;