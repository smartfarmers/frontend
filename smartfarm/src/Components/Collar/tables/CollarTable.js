import React from 'react'

const CollarTable = props => (
  <table className = "collarTable" >
    <thead className = "collarHead">
      <tr className = "collarTr">
        <th className = "collarTh"><h6>Collar id</h6></th>
        <th className = "collarTh"><h6>Serial</h6></th>
        <th className = "collarTh"><h6>Collar Number</h6></th>
        <th className = "collarTh"><h6>Date of Entry</h6></th>
      </tr>
    </thead>
    <tbody>
      {props.collars.length > 0 ? (
        props.collars.map(collar => (
          <tr key={collar.id}>
            <td className = "collarTd">{collar.collar_id}</td>
            <td className = "collarTd">{collar.serialNo}</td>
            <td className = "collarTd">{collar.collarNo}</td>
            <td className = "collarTd">{collar.dateOfEntry}</td>  
            <td className = "collarTd">
              <button type="collar-submit" onClick={() => {
                  props.editRow(collar)}} > Edit </button>&nbsp;
              <button type="collar-submit" onClick={() => { 
                  if (window.confirm('Are you sure you wish to delete this collar?')) props.deleteCollar(collar)}}
                  disabled={Boolean(collar.canDelete === false)}>Delete</button>
            </td>
          </tr>
        ))
      ) : (
        <tr>
          <td className = "collarTd" colSpan={3}>No collars</td>
        </tr>
      )}
    </tbody>
  </table>
)

export default CollarTable

