import React, { Fragment, Component } from 'react'
import AddCollarForm from './forms/AddCollarForm'
import EditCollarForm from './forms/EditCollarForm'
import CollarTable from './tables/CollarTable'
import Error from '../Errors/Error'
import './Collar.css'
import { Redirect } from 'react-router-dom'; 

class Collar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collarData: [],
      collarGetResponse: [],
      collarPostResponse: null,
      currentCollar: null,
      editing: null,
      setEditing: '',
      getError: null,
      postError: null,
      putError: null,
      deleteError: null
    }
    this.addCollar = this.addCollar.bind(this)
    this.deleteCollar = this.deleteCollar.bind(this)
    this.editRow = this.editRow.bind(this)
    this.updateCollar = this.updateCollar.bind(this)
    this.mapGetCollarResponse = this.mapGetCollarResponse.bind(this)
    this.mapNewCollar = this.mapNewCollar.bind(this)
    this.getCollarList = this.getCollarList.bind(this)
    this.mapUpdateCollar = this.mapUpdateCollar.bind(this)
  }

  componentDidMount(){
    this.getCollarList()
  }

  getCollarList(){
    // fetches collars
    fetch(`${process.env.REACT_APP_API_URL}/collar/`, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
      .then(res => res.json())
      .then(json => {
        if(json.status !== 200){
          this.setState({
            collarGetResponse: json,
            getError: json.status
          })
        } else {
          let mappedResponse = this.mapGetCollarResponse(json.response)
          this.setState({
              collarGetResponse: json,
              collarData: mappedResponse
        })
        }
    }).catch(error => {
      this.setState({
        getError: error
      })
    });
  }

  mapGetCollarResponse(response) {
    let result = []
    var id = 0
    var canDelete = true
    response.forEach(element => {
      let item = {
        id: id,
        collar_id: element["COLLAR_ID"],
        serialNo: element["COLLAR_SER_NO"],
        collarNo: element["COLLAR_NUM"],
        dateOfEntry: element["DATE_OF_ENTRY"],
        collLocId: element["collar_loc_id"],
        canDelete: canDelete
      }
      if (item.collLocId) {
        item.canDelete = false
      }
      if (result.length > 0){
        if (item.collar_id === result[result.length-1].collar_id) {
          result.pop()
        }
      }
      result.push(item)
      id++
    });
    return result
  }

	// CRUD operations
	addCollar(collar) {
    let newCollar = this.mapNewCollar(collar);
    fetch(`${process.env.REACT_APP_API_URL}/collar`, {
      method: 'post',
      headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
      body: JSON.stringify(newCollar)
    }).then(res => res.json())
      .then(json => {
        this.setState({
            collarPostResponse: json,
      }, () => {
        if(json.status !== 201 ){
          this.setState({
            postError: json.status
          })
        }
        this.getCollarList()
      })
    }).catch(error => {
      this.setState({
        postError: error
      })
    });
  }
  
  mapNewCollar(collar) {
    let item = {
      collar_id: collar["collar_id"],
      collar_ser_no: collar["serialNo"],
      collar_num: collar["collarNo"],
    }
    return item
  }

  mapUpdateCollar(collar) {
    let item = {
      collar_ser_no: collar["serialNo"],
      collar_num: collar["collarNo"],
    }
    return item
  }

	deleteCollar(collar) {
    fetch(`${process.env.REACT_APP_API_URL}/collar/` + collar["collar_id"], {
      method: 'delete',
      headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}
    }).then(res => res.json())
      .then(json => {
        if(json.status != 200 ){
          this.setState({
            deleteError: json
          })
        }
        this.setState({
            collarDeleteResponse: json,
      }, () => {
        this.getCollarList()
      })
    }).catch(error => {
      this.setState({
        deleteError: error
      })
    });
	}

	updateCollar(id, updatedCollar) {
    this.setState({
      editing: false,
      collarData: this.state.collarData.map(collar => (collar.id === id ? updatedCollar : collar))
    })

    let mappedCollar = this.mapUpdateCollar(updatedCollar);
    fetch(`${process.env.REACT_APP_API_URL}/collar/` + updatedCollar.collar_id, {
      method: 'put',
      headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
      body: JSON.stringify(mappedCollar)
    }).then(res => res.json())
      .then(json => {
        this.setState({
            collarPutResponse: json,
      }, () => {
        this.getCollarList()
      })
    }).catch(error => {
      this.setState({
        putError: error
      })
    });
  }

	editRow(collar) {
    this.setState({
      editing: true,
      currentCollar: { id: collar.id, collar_id: collar.collar_id, serialNo: collar.serialNo, collarNo: collar.collarNo, dateOfEntry: collar.dateOfEntry }
    })
  }
  
  getError(){
    if(this.state.getError){
      return (
        <Error
          errorType="error"
          errorHeading="Unable to retrieve list of collars" 
          errorMessage="We were unable to obtain the list of collars, please contact application administrator">
        </Error>
      )
    }
  }

  putError(){
    if(this.state.putError){
      return (
        <Error
          errorType="error"
          errorHeading="Unable to update collar" 
          errorMessage="We were unable to update collar, please contact application administrator">
        </Error>
      )
    }
  }

  deleteError(){
    if (this.state.deleteError) {
      if (this.state.deleteError.error != null ){
        if (this.state.deleteError.error.includes("Collar allocation exists")){
          return (
            <Error
              errorType="error"
              errorHeading="Unable to delete collar due to a collar allocation" 
              errorMessage="We were unable to delete collar, due to the collar being allocated. Please remove the allocation and try again.">
            </Error>
          )
        }
      }
      return (
        <Error
          errorType="error"
          errorHeading="Unable to delete collar" 
          errorMessage="We were unable to delete collar, please contact application administrator">
        </Error>
      )
    }
  }

  postError(){
    if (this.state.postError) {
      return (
        <Error
          errorType="error"
          errorHeading="Unable to create collar" 
          errorMessage="We were unable to create collar, please contact application administrator">
        </Error>
      )
    } 
  }

	render() {
    if (!localStorage.getItem('token')) {
      return (
          <Redirect to={{ pathname: '/' }} push/>
      ) 
    }
    return (
      <div className="collar-container">
        <h1 className="CollarHeading">Collar Inventory</h1>
        <div className="collar-flex-row">
          <div className="collar-flex-large">
            {this.state.editing ? (
              <Fragment>
                <h2>Edit collar</h2>
                <EditCollarForm
                  editing={this.state.editing}
                  setEditing={this.state.setEditing}
                  currentCollar={this.state.currentCollar}
                  updateCollar={this.updateCollar}
                />
              </Fragment>
            ) : (
              <Fragment>
                <h2>Add Collar</h2>
                <AddCollarForm addCollar={this.addCollar} />
              </Fragment>
            )}
          </div>
          <div className="collar-flex-large">
            <h2>View Collars</h2>
            <CollarTable collars={this.state.collarData} editRow={this.editRow} deleteCollar={this.deleteCollar} />
          </div>
        </div>
        <div className="flex-row collar-errors">
          { this.getError() }
          { this.putError() }
          { this.postError() }
          { this.deleteError() }
        </div>
      </div>
    ) 
  }
}

export default Collar;

