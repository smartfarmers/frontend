import React, { useState, useEffect } from 'react'

const EditCollarForm = props => {
  const [ collar, setCollar ] = useState(props.currentCollar)
  
  useEffect(
    () => {
      setCollar(props.currentCollar)
    },
    [ props ]
  )
  
  const handleInputChange = event => {
    const { name, value } = event.target

    setCollar({ ...collar, [name]: value })
    
  }

  return (
    <div>
        <div>
            <h6>Collar Serial Number</h6>
            <input type="text" name="serialNo" value={collar.serialNo} onChange={handleInputChange} />

            <h6>Collar Number</h6>
            <input type="text" name="collarNo" value={collar.collarNo} onChange={handleInputChange} />

            <button type="collar-submit" onClick={() => {
                props.updateCollar(collar.id, collar)}}>
                Update collar
            </button>&nbsp;

            <button type="collar-submit" onClick={() => { 
                window.location.reload()} } >
                Cancel
            </button>
        </div>
        
    </div>
  )
}

export default EditCollarForm

