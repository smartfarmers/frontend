import React, { useState } from 'react'

const AddCollarForm = props => {
	const initialFormState = { id: null, collar_id: '', serialNo: '', collarNo: '' }
	const [ collar, setCollar ] = useState(initialFormState)

	const handleInputChange = event => {
		const { name, value } = event.target
		setCollar({ ...collar, [name]: value })
	}

	return (
		<form
			onSubmit={event => {
				event.preventDefault()
				if (!collar.serialNo || !collar.collarNo || !collar.collar_id ) return 

				props.addCollar(collar)
				setCollar(initialFormState)
			}}
		>
			<h6>Collar id</h6>	
			<input type="text" name="collar_id" value={collar.collar_id} onChange={handleInputChange} pattern="[A-Za-z0-9_]+" placeholder="03_268160"/>
			<span className="ValidationRules">Only digits, letters and underscores allowed.</span>

			<h6>Collar Serial Number</h6>
			<input title="Collar Serial Number" type="text" name="serialNo" value={collar.serialNo} onChange={handleInputChange} pattern="[A-Za-z0-9]+" placeholder="748393984J"/>

			<h6>Collar Number</h6>
			<input title="Collar Number" type="text" name="collarNo" value={collar.collarNo} onChange={handleInputChange} pattern="[A-Za-z0-9]+" placeholder="s18"/>

			<button type="collar-submit">Add new collar</button>
			
			
		</form>
	)
}

export default AddCollarForm
