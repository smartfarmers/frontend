import React from "react";
import './UploadStatus.css';

class UploadStatus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        uploadFiles: []
    }
  }

  // fetches temperature with cow_id for a specified date
  componentDidMount(){
    fetch(`${process.env.REACT_APP_API_URL}/uploadingFiles/`,
    {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
      .then(res => res.json())
      .then(json => {
       this.setState({
         isLoaded: true,
         uploadFiles: json.response, 
       }) 
    }); 
  }

  renderTableData() {
    return this.state.uploadFiles.map((data, i) => {
        console.log(data)
      return (
        <tr>
            <td>{data.FILE_NAME}</td>
            <td>{data.IMPORT_PERCENT}</td>
        </tr>
      );
    })
  }
  
  render() {
    var {isLoaded, uploadFiles } = this.state;
    if (!isLoaded) {
      return <div>Loading...File Upload Statues</div>;
    } else if(uploadFiles.length === 0){
      return (
        <div id="NoFileUploadStatus">
            <h3>No files currently being imported</h3>
        </div>
      );
    } else{
      return (
        <div id="FileUploadStatus">
            <h3>Files currently being imported</h3>
            <table id="FileUploadStatusTable">
                <thead>
                    <tr>
                        <td>File Name:</td>
                        <td>Percent Complete:</td>
                    </tr>
                </thead>
                <tbody>
                    {this.renderTableData()}
                </tbody>
            </table>
        </div>
      );
    }
  }
}
export default UploadStatus;
