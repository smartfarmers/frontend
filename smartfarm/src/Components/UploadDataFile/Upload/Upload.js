import React, { Component } from "react";
import Dropzone from '../Dropzone/Dropzone'
import "./Upload.css";
import Progress from "../Progress/Progress";
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import socketIOClient from "socket.io-client";
import Error from '../../Errors/Error'

class Upload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      files: [],
      uploading: false,
      uploadProgress: {},
      importing: false,
      importProgress: {},
      successfullUploaded: false,
      successfullImported: false,
      uploadError: [],
      importError: []
    };

    this.onFilesAdded = this.onFilesAdded.bind(this);
    this.uploadFiles = this.uploadFiles.bind(this);
    this.sendRequest = this.sendRequest.bind(this);
    this.renderActions = this.renderActions.bind(this);
    this.triggerFileImport = this.triggerFileImport.bind(this);
  }

  onFilesAdded(files) {
    this.setState(prevState => ({
      files: prevState.files.concat(files)
    }));
  }

  triggerFileImport(fileName){
    this.state.importing = true;
    const socket = socketIOClient(`${process.env.REACT_APP_SOCKET_URL}`);
    socket.emit("fileImport", fileName)
    socket.on("fileImport", data => {
      if(data.includes("uploaded")){
        this.handleUploadProgress(data);
      } else if(data.includes("progress")){
        this.handleImportProgress(data);
      } else if(
        data.includes("Unable to find a collar ID") ||
        data.includes("No cow has currently been allocated") ||
        data.includes("ERROR: Unable to determine the file type") ||
        data.includes("already imported")
        ){
        this.setState({
          importError: [ ...this.state.importError, data.trim()]
        })
      }
    });
  }

  handleUploadProgress(data){
    let uploadedFileName = data.split(" ")[0]
    this.state.importing = false
    const copy = { ...this.state.importProgress };
    copy[uploadedFileName] = { state: "done", percentage: 100 };
    this.setState({ importProgress: copy });
    this.setState({ successfullImported: true })
  }

  handleImportProgress(data){
    let fileName = data.split(" ")[0]
    let progress = data.split(" ")[2];
    const copy = { ...this.state.importProgress };
    copy[fileName] = {
      state: "pending",
      percentage: parseFloat(progress) * 100
    };
    this.setState({ importProgress: copy });
  }

  async uploadFiles() {
    this.setState({ uploadProgress: {}, uploading: true });
    const promises = [];
    this.state.files.forEach(file => {
      promises.push(this.sendRequest(file));
    });
    try {
      await Promise.all(promises);
      this.setState({ successfullUploaded: true, uploading: false });
    } catch (e) {
      // Not Production ready! Do some error handling here instead...
      this.setState({ 
          successfullUploaded: true,
          uploading: false,
          uploadError: [ ...this.state.uploadError, "Upload Failed"]
        });
    }
  }

  sendRequest(file) {
    return new Promise((resolve, reject) => {
      const req = new XMLHttpRequest();

      req.upload.addEventListener("progress", event => {
        if (event.lengthComputable) {
          const copy = { ...this.state.uploadProgress };
          copy[file.name] = {
            state: "pending",
            percentage: (event.loaded / event.total) * 100
          };
          this.setState({ uploadProgress: copy });
        }
      });

      req.upload.addEventListener("load", event => {
        const copy = { ...this.state.uploadProgress };
        copy[file.name] = { state: "done", percentage: 100 };
        this.setState({ uploadProgress: copy });
        this.triggerFileImport(file.name);
        resolve(req.response);
      });

      req.upload.addEventListener("error", event => {
        const copy = { ...this.state.uploadProgress };
        copy[file.name] = { state: "error", percentage: 0 };
        this.setState({
          uploadProgress: copy,
          uploadError: [ ...this.state.uploadError, `Upload Failed for file ${file.name}`]
        });
        reject(req.response);
      });

      const formData = new FormData();
      formData.append("file", file, file.name);

      req.open("POST", `${process.env.REACT_APP_API_URL}/upload`);
      req.send(formData);
    });
  }

  renderProgress(file) {
    const uploadProgress = this.state.uploadProgress[file.name];
    if (this.state.uploading || this.state.successfullUploaded) {
      return (
        <div className="ProgressWrapper">
          <span className="status">Upload: </span>
          <Progress progress={uploadProgress ? uploadProgress.percentage : 0} />
          <CheckCircleIcon
            className="CheckIcon"
            alt="done"
            style={{
              opacity:
                uploadProgress && uploadProgress.state === "done" ? 0.5 : 0
            }}
          />
        </div>
      );
    }
  }

  renderImportProgress(file) {
    const importProgress = this.state.importProgress[file.name];
    if (this.state.importing || this.state.successfullImported) {
      return (
        <div className="ProgressWrapper">
          <span className="status">Import: </span>
          <Progress progress={importProgress ? importProgress.percentage : 0} />
          <CheckCircleIcon
            className="CheckIcon"
            alt="done"
            style={{
              opacity:
              importProgress && importProgress.state === "done" ? 0.5 : 0
            }}
          />
      </div>
      )
    }
  }

  renderActions() {
    if (this.state.successfullUploaded) {
      return (
        <button type="upload-submit"
          onClick={() =>
            this.setState(
              { 
                files: [], 
                successfullUploaded: false,
                importError: [],
                uploadError: []
              })
          }
        >
          Clear
        </button>
      );
    } else {
      return (
        <button type="upload-submit"
          disabled={this.state.files.length < 0 || this.state.uploading}
          onClick={this.uploadFiles}
        >
          Upload
        </button>
      );
    }
  }

  renderImportErrors() {
    if (this.state.importError.length > 0) {
      let errorList = this.state.importError.filter( this.onlyUnique )
      return (
        <div className="ImportErrors">
          {errorList.map( (element, index) => {
            return (
              <Error
              errorType="error"
              errorHeading="Import Error" 
              errorMessage={element}>
            </Error>
            )
          })}
        </div>
      );
    }
  }

  renderUploadErrors() {
    if (this.state.uploadError.length > 0) {
      let errorList = this.state.uploadError.filter( this.onlyUnique )
      return (
        <div className="UploadErrors">
          {errorList.map( (element, index) => {
            return (
              <Error
              errorType="error"
              errorHeading="Upload Error" 
              errorMessage={element}>
            </Error>
            )
          })}
        </div>
      );
    }
  }

  onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}

  render() {
    return (
      <div className="Upload">
        <span className="Title">Upload Files</span>
        <div className="Content">
          <div>
            <Dropzone
              onFilesAdded={this.onFilesAdded}
              disabled={this.state.uploading || this.state.successfullUploaded}
            />
          </div>
          <div className="Files">
            {this.state.files.map(file => {
              return (
                <div key={file.name} className="Row">
                  <span className="Filename">{file.name}</span>
                  {this.renderProgress(file)}
                  {this.renderImportProgress(file)}
                </div>
              );
            })}
          </div>
        </div>
        <div className="Actions">{this.renderActions()}</div>
        {this.renderImportErrors()}
        {this.renderUploadErrors()}
      </div>
    );
  }
}

export default Upload;