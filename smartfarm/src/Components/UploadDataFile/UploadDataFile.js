import React, { Component } from 'react'
import Upload from './Upload/Upload'
import UploadFileStatus from './UploadStatus/UploadStatus'
import './UploadDataFile.css'
import { Redirect } from 'react-router-dom'; 

class UploadDataFile extends Component {
    render() {
        if (!localStorage.getItem('token')) {
            return (
                <Redirect to={{ pathname: '/' }} push/>
            ) 
        } 
        return (
            <div className="UploadDataFile">
                <div className="UploadCard">
                    <Upload />
                </div>
                <div className="UploadStatusTable">
                    <UploadFileStatus />
                </div>
            </div>
        )
        
    }
}

export default UploadDataFile