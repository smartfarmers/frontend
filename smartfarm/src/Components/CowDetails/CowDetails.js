import React from "react";
import Table from "../CommonComponents/Table/Table";
import './CowDetails.css';

class CowDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [], // for weight, collar_id
      tempDay: [],
      isLoaded: false,
      cowId: '',
      date:'',
      headers: ["CowId", "CollarId", "Weight", "WeighDate", "WeighedBy"],
      loadedCowData: []
    }

    this.mapResponse = this.mapResponse.bind(this);
  }

  // using cowlist temporarily, instead of weight(cause weight not in DB / API calls stopped)
  componentDidMount(){
    
    this.setState({
      items: this.props.items
    }, () => {
      this.state.items.forEach( element => {
        fetch(`${process.env.REACT_APP_API_URL}/weight/` + element, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
        .then(res => res.json())
        .then(json => {
          var cowDetails = this.mapResponse(json.response, element)
          var res = this.state.loadedCowData
          res.push(cowDetails)
          this.setState({
            loadedCowData: res
          })
        });
      })
      this.setState({
        isLoaded: true
      });
    })
  }

  mapResponse(response, cowId) {
    if(response === null || response.length === 0){
      let res = [
        cowId,
        "No value",
        "No value",
        "No value",
        "No value",
      ]
      return res
    }
    let res = [
      cowId,
      response[0]["collar_id"],
      `${response[0]["weight_kg"]}kg`,
      response[0]["weigh_date"],
      response[0]["weighed_by"]
    ]
    return res
  } 

  render() {
    var {isLoaded, headers } = this.state;
    if (!isLoaded) {
      return <div>Loading...cowlist</div>;
    }
    
    else{
      var { loadedCowData } = this.state;
      return (
        <div id="CowDetailsWrapper">
            <Table 
              headers={headers} 
              tableData={loadedCowData}
            />
        </div>  
        
      );
    }
  }
}

export default CowDetails;
