import React, { Component } from "react";
import "./CompareCowSelectionDetails.css";
import CompareCowSingleDate from '../CompareCowDateDetails/CompareCowSingleDate';
import CompareCowRangeDate from '../CompareCowDateDetails/CompareCowRangeDate';
import MultiSelect from "@khanacademy/react-multi-select";

class CompareCowSelectionDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoaded: false,
        cowIdGroup1: [],
        cowIdGroup2: [],
        dateSelector: '',
        cowOptions: []
    }
    this.handleCheck = this.handleCheck.bind(this);
    this.remapCowList = this.remapCowList.bind(this); 
  }

  componentDidMount(){
    fetch(`${process.env.REACT_APP_API_URL}/cowlist`, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
      .then(res => res.json())
      .then(json => {
       this.setState({
         isLoaded: true,
         cowList: json.response, 
       })
       this.remapCowList()
    }); 
  }

  remapCowList() {
    var res = []
    this.state.cowList.map((cow) => {
      res.push({
        label: cow.cow_id,
        value: cow.cow_id
      })
    })
    this.setState({
      cowOptions: res
    })
  }

  handleCheck(event){
    if(event.target.value === "singleDay"){
      this.setState({dateSelector: "singleDay"})
    } else if(event.target.value === "range"){
      this.setState({dateSelector: "range"})
    }
  }


  renderSelectionOrder() {
    if(this.state.cowIdGroup1.length === 0 || this.state.cowIdGroup2.length === 0){
      return(<span>Please select a CowID for both group 1 and group 2</span>)
    } else { 
      return(<div></div>)
    }
  }

  renderDatePicker() {
    if(this.state.cowIdGroup1.length === 0 || this.state.cowIdGroup2.length === 0){
      return(<div></div>)
    } else {
      if(this.state.dateSelector === "singleDay"){
        return (
          <CompareCowSingleDate 
            range="single" 
            compareCow="true" 
            cowIdGroup1={this.state.cowIdGroup1}
            cowIdGroup2={this.state.cowIdGroup2}
          />
        )
      } else if (this.state.dateSelector === "range") {
        return (
          <CompareCowRangeDate 
            range="range" 
            compareCow="true" 
            cowIdGroup1={this.state.cowIdGroup1}
            cowIdGroup2={this.state.cowIdGroup2}
          />
        )
      }
    }
  }

  render() {
    const {cowIdGroup1, cowIdGroup2} = this.state;
    var {isLoaded } = this.state;
    if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="CompareCowSelectionDetails">
          { this.renderSelectionOrder() }
          <form>
            <div>
              <label className="CowIdSelectionLabel">Select Cow Group 1:</label>
              <MultiSelect
                className="MultiCowSelect"
                options={this.state.cowOptions}
                selected={cowIdGroup1}
                onSelectedChanged={cowIdGroup1 => this.setState({cowIdGroup1})}
              />
            </div>
            <div>
              <label className="CowIdSelectionLabel">Select Cow Group 2:</label>
              <MultiSelect
                className="MultiCowSelect"
                options={this.state.cowOptions}
                selected={cowIdGroup2}
                onSelectedChanged={cowIdGroup2 => this.setState({cowIdGroup2})}
              />
            </div>
              <div className="CompareCowDateRangeSelector">
                  <label className="CompareCowSelectRangeLabel">Select Range:</label>
                    <div className="CompareCowRangeSelectors">
                    <label>
                        <input
                          name="dateSelector"
                          value="singleDay"
                          id="CompareCowSingleDay"
                          type="radio"
                          className="SingleCheckBox"
                          checked={this.state.singleDay}
                          onChange={this.handleCheck} />
                        Single Day
                </label>
                <br />
                <label className="CompareCowSelectRangeLabel">
                    <input
                      name="dateSelector"
                      value="range"
                      type="radio"
                      id="CompareCowRange"
                      className="DateRangeCheckBox"
                      checked={this.state.range}
                      onChange={this.handleCheck}
                        />
                    Date Range
                </label>
                </div>
              </div>
            { this.renderDatePicker() }
          </form>     
        </div>
        
      );
    }
  }
}

export default CompareCowSelectionDetails;