import React, { Component } from 'react';
import CompareCowTempDay from '../CompareCowTempDay/CompareCowTempDay';
import CowDetails from '../../CowDetails/CowDetails';
import CompareCowActivityDay from '../CompareCowActivityDay/CompareCowActivityDay';
import CompareCowMap from '../CompareCowMap/CompareCowMap';
import './CompareCowDayDisplay.css';

class CompareCowDayDisplay extends Component {
    constructor(props){
        super(props)
        this.state = {
          cowIdGroup1: null,
          cowIdGroup2: null,
          group1StartDate: null,
          group2StartDate: null,
    }
  }

  componentDidMount(){
    const { cowIdGroup1 } = this.props.location.state
    const { cowIdGroup2 } = this.props.location.state
    const { group1StartDate } = this.props.location.state
    const { group2StartDate } = this.props.location.state
    this.setState({
      cowIdGroup1: cowIdGroup1,
      cowIdGroup2: cowIdGroup2,
      group1StartDate: this.formatDate(group1StartDate),
      group2StartDate: this.formatDate(group2StartDate),
    })
  }

    formatDate(date){
        let formattedDate = date.toISOString()
        formattedDate = formattedDate.split("T")[0]
        return formattedDate
      }

    render() {
        if(this.state.cowIdGroup1 && this.state.group1StartDate){
            return (
                <div className="CompareCowDayDisplay">
                    <h1 className="CompareCowDaySearchTitle">Compare Cow</h1>
                    <CompareCowActivityDay cowIdGroup1={this.state.cowIdGroup1} cowIdGroup2={this.state.cowIdGroup2} group1Date={this.state.group1StartDate} group2Date={this.state.group2StartDate} /> 
                    <div className="CompareCowDayDisplayWrapper">
                      <div className="CompareCowDayDisplayGraphWrapper">
                        <CompareCowTempDay cowIdGroup1={this.state.cowIdGroup1} cowIdGroup2={this.state.cowIdGroup2} group1Date={this.state.group1StartDate} group2Date={this.state.group2StartDate} />
                        <CompareCowMap cowIdGroup1={this.state.cowIdGroup1} cowIdGroup2={this.state.cowIdGroup2} group1Date={this.state.group1StartDate} group2Date={this.state.group2StartDate} /> 
                      </div>
                      <div className="CompareCowDayDetailsWrapper">
                        <div>
                          <h2 className="CompareCowGroupTitle">Group 1 Details:</h2>
                          <CowDetails items={this.state.cowIdGroup1} />
                        </div>
                        <div>
                          <h2 className="CompareCowGroupTitle">Group 2 Details:</h2>
                          <CowDetails className="compareCowDetails" items={this.state.cowIdGroup2} />
                        </div>
                      </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="CompareCowDayDisplay">
                    <span className="CompareCowLoading">Loading...</span>
                </div>
            )
        }
    }
}

export default CompareCowDayDisplay