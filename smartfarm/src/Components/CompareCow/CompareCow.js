import React, { Component } from 'react';
import './CompareCow.css';
import CompareCowSelectionDetails from './CompareCowSelectionDetails/CompareCowSelectionDetails';
import { Redirect } from 'react-router-dom'; 

class CompareCow extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            cowId: '',
        }
      }

    render() {
        if (!localStorage.getItem('token')) {
            return (
                <Redirect to={{ pathname: '/' }} push/>
            ) 
        }
        return (
            <div className="CompareCow">
                <div className="CompareCowCard">  
                    <h1 className="SearchTitle">Compare Cows</h1>
                    <CompareCowSelectionDetails />
                </div>
            </div>
        )
    }
}

export default CompareCow