import React, { Component } from 'react';
import { ReactBingmaps } from 'react-bingmaps';
import './CompareCowMapRange.css';

class CompareCowMapRange extends Component {
    constructor(props) {
        super(props);
        this.state = {
          group1Loaded: false,
          group2Loaded: false,
          allDataLoaded: false,
          cowIdGroup1: [],
          cowIdGroup2: [],
          group1Results: [],
          group2Results: [],
          combinedResults: [],
          amountOfDays: 0,
        }
      }

      handlePinOnHover(event){
      }

    // fetches location with cow_id for a specified date
    componentDidMount(){
      this.setState({
        cowIdGroup1: this.props.cowIdGroup1,
        cowIdGroup2: this.props.cowIdGroup2,
        group1Date: this.props.group1Date,
        group2Date: this.props.group2Date,
        amountOfDays: this.props.amountOfDays
      }, async () => {
        await this.getLocRangeData(this.state.cowIdGroup1, 
          this.state.cowIdGroup2,  
          this.state.group1Date, 
          this.state.group2Date, 
          this.state.amountOfDays
        );
      })
    }

    async getLocRangeData(cowIdGroup1, cowIdGroup2,  group1Date, group2Date, amountOfDays){
      let group1StartDate = new Date(group1Date);
      let group1EndDate = new Date()
      group1EndDate.setDate(group1StartDate.getDate() + amountOfDays);
      group1EndDate = this.formatDate(group1EndDate);

      let request = { 
        cowGroup: cowIdGroup1
      }
      fetch((`${process.env.REACT_APP_API_URL}/grouplocrange/` + group1Date + '/' + group1EndDate), {
        method: 'post',
        headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
        body: JSON.stringify(request)
      }) 
        .then(res => res.json())
        .then(json => {
          if(json.response.length !== 0){
              this.setState({
                  group1Results: this.parseCowLocData(json.response, 1)
              })
          }
          this.setState({
              group1Loaded: true,
          })
      });

      let group2StartDate = new Date(group2Date);
      let group2EndDate = new Date()
      group2EndDate.setDate(group2StartDate.getDate() + amountOfDays);
      group2EndDate = this.formatDate(group2EndDate);
      request = { 
        cowGroup: cowIdGroup2
        }
      fetch((`${process.env.REACT_APP_API_URL}/grouplocrange/` + group2Date + '/' + group2EndDate), {
        method: 'post',
        headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
        body: JSON.stringify(request)
      }) 
        .then(res => res.json())
        .then(json => {
          if(json.response.length !== 0){
              this.setState({
                  group2Results: this.parseCowLocData(json.response, 2)
              })
          }
          this.setState({
              group2Loaded: true,
              combinedResults: this.state.group1Results.concat(this.state.group2Results)
          })
      });
      }
    
      formatDate(date){
        let formattedDate = date.toISOString()
        formattedDate = formattedDate.split("T")[0]
        return formattedDate
      }

      parseCowLocData(res, group){
        // Make the mouse over event for the pins display the date time of the pin and cow id
        let result = []
        res.forEach( response => {
            if(group === 1){
                response.forEach(element => {
                    if(element["loc_lat"] != null || element["loc_lon"] != null){
                        let pin = {
                            "location" : [
                                element["loc_lat"],
                                element["loc_lon"]
                            ],
                            "pushPinOption":{ title: 'Pushpin Title', description: 'Pushpin' },
                            "option" : {
                                "color" : "red"
                            },
                            "addHandler" : {
                                "type" : "mouseover",
                                callback : this.handlePinOnHover()
                            }
                        };
                        result.push(pin);
                    }
                });
            } else {
                response.forEach(element => {
                    if(element["loc_lat"] != null || element["loc_lon"] != null){
                        let pin = {
                            "location" : [
                                element["loc_lat"],
                                element["loc_lon"]
                            ],
                            "option" : {
                                "color" : "blue"
                            },
                            "addHandler" : {
                                "type" : "mouseover",
                                callback : this.handlePinOnHover()
                            }
                        };
                        result.push(pin);
                    }
                });
            }
        })
        return result;
    }

    render() {
      var {group1Loaded, group2Loaded } = this.state;
      if (!group2Loaded || !group1Loaded) {
          return <div className="CompareCowLoading">Loading... Location Data</div>;
        } else if(this.state.combinedResults.length === 0){
          return (
            <div id="CompareCowMapRange"> 
              <span className="CompareCowNoData">No Location data</span>
            </div>
         )
        } else {
            return (
                <div className="CompareCowMapRange">
                    <h1 className="CowMapHeading">CowMap</h1>
                    <ReactBingmaps 
                        bingmapKey = "Al4i0oHFLW-z-hDROXL89Wp37vGOgqMJT6aX6ompovuUH9Ef54yzbBuGdyWhRtEe"
                        center = {[-35.0479, 147.312]}
                        mapTypeId= {"aerial"}
                        navigationBarMode={"minified"}
                        pushPins={this.state.combinedResults}
                    />
                    <div className="CompareCowMapKeyWrapper">
                      <div className="CompareCowKeyDiv">
                        <span>Group 1 :</span>
                        <div className="CompareCowMapKeyGroup1"></div>
                      </div>
                      <div className="CompareCowKeyDiv">
                        <span>Group 2 :</span>
                        <div className="CompareCowMapKeyGroup2"></div>
                      </div>
                    </div>
                </div>
            )
        }
    }
}

export default CompareCowMapRange