import React, { Component } from "react";
import { Link } from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import './CompareCowDateDetails.css';

class CompareCowRangeDate extends Component {
    constructor(props) {
      super(props);
      this.state = {
        cowIdGroup1 : [],
        cowIdGroup2: [],
        range : '',
        group1StartDate : new Date(),
        group2StartDate : new Date(),
        amountOfDays: 0
      };
    }
  
    componentDidMount(){
      this.setState({
        cowIdGroup1: this.props.cowIdGroup1,
        cowIdGroup2: this.props.cowIdGroup2,
        range: this.props.range
      })
    }
  
    handleChange = date => {    
      this.setState({
        group1StartDate: date
      });
    };
  
    handleChangeSecondDate = date => {    
      this.setState({
        group2StartDate: date
      });
    };
  
    handleChangeAmount = amount => {
      this.setState({
        amountOfDays: amount.target.value
      });
    };
  
    render() {
      return (
        <div className="CompareCowRangeDateDetails">
          <h1>Date Range Selector</h1>
          <label>First Day for Group1: </label>
          <DatePicker
            className="startDatePicker"
            selected={this.state.group1StartDate}
            onChange={this.handleChange}
          />
          <label>First Day for Group2: </label>
          <DatePicker
            className="endDatePicker"
            selected={this.state.group2StartDate}
            onChange={this.handleChangeSecondDate}
          />
          <label>The amount of days: </label>
          <input
            className="CompareCowAmountOfDays"
            onChange={this.handleChangeAmount}
            type="number"
            max="7"
            min="0"
          />
          <Link 
            to={{
              pathname: `/CompareCowRangeDisplay`,
              state: {
                cowIdGroup1: this.state.cowIdGroup1,
                cowIdGroup2: this.state.cowIdGroup2,
                group1StartDate: this.state.group1StartDate,
                group2StartDate: this.state.group2StartDate,
                amountOfDays: this.state.amountOfDays
              }
            }}
          >
            <button className="CompareCowRangeSubmit">Submit</button>
          </Link>
        </div>
      );
    }
  }
  
  export default CompareCowRangeDate;