import React, { Component } from "react";
import { Link } from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import './CompareCowDateDetails.css';

class CompareCowSingleDate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cowIdGroup1 : [],
      cowIdGroup2: [],
      range : '',
      group1StartDate : new Date(),
      group2StartDate : new Date(),
    };
  }

  componentDidMount(){
    this.setState({
      cowIdGroup1: this.props.cowIdGroup1,
      cowIdGroup2: this.props.cowIdGroup2,
      range: this.props.range
    })
  }

  handleChange = date => {    
    this.setState({
      group1StartDate: date
    });
  };

  handleChangeSecondDate = date => {    
    this.setState({
      group2StartDate: date
    });
  };

  handleChangeAmount = amount => {
    this.setState({
      amountOfDays: amount.target.value
    });
  };

  render() {
    return (
    <div className="CompareCowSingleDateDetails">
        <h1>Single Date Selector</h1>
        <label>Select Day for Group1</label>
        <DatePicker
        className="startDatePicker"
        selected={this.state.group1StartDate}
        onChange={this.handleChange}
        />
        <label>Select Day for Group2:</label>
        <DatePicker
        className="startDatePicker"
        selected={this.state.group2StartDate}
        onChange={this.handleChangeSecondDate}
        />
        <Link 
        to={{
            pathname: `/CompareCowDayDisplay`,
            state: {
            cowIdGroup1: this.state.cowIdGroup1,
            cowIdGroup2: this.state.cowIdGroup2,
            group1StartDate: this.state.group1StartDate,
            group2StartDate: this.state.group2StartDate,
            }
        }}
        >
        <button className="CompareCowRangeSubmit">Submit</button>
        </Link>
    </div>
    );
  }
}

export default CompareCowSingleDate;