import React from "react";
import './CompareCowTempDay.css';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,} from 'recharts';

class CompareCowTempDay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      group1Loaded: false,
      group2Loaded: false,
      results: [],
    }
  }

  // fetches temperature with cow_id for a specified date
  componentDidMount(){
    this.setState({
      cowIdGroup1: this.props.cowIdGroup1,
      cowIdGroup2: this.props.cowIdGroup2,
      group1Date: this.props.group1Date,
      group2Date: this.props.group2Date
    }, async () => {
        let request = { 
          cowGroup: this.state.cowIdGroup1
        }
        await fetch((`${process.env.REACT_APP_API_URL}/grouptempdate/` + this.props.group1Date), {
          method: 'post',
          headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
          body: JSON.stringify(request)
        }) 
        .then(res => res.json())
        .then(json => {
          if(json.response.length !== 0){
            this.setState({
              group1Results: json.response
            })
          } else {
            this.setState({
              group1Results: [{
                '00_00': 0,
                '04_00': 0,
                '08_00': 0,
                '12_00': 0,
                '16_00': 0,
                '20_00': 0,
                MAX: 0,
                TEMP_DATE: this.props.group1Date
              }],
              noGroup1Results: true
            })
          }
          this.setState({
            group1Loaded: true, 
          })
        }); 

        request = { 
          cowGroup: this.state.cowIdGroup2
        }
        await fetch((`${process.env.REACT_APP_API_URL}/grouptempdate/` + this.props.group2Date), {
          method: 'post',
          headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
          body: JSON.stringify(request)
        }) 
        .then(res => res.json())
        .then(json => {
          if(json.response.length !== 0){
            this.setState({
              group2Results: json.response
            })
          } else {
            this.setState({
              group2Results: [{
                '00_00': 0,
                '04_00': 0,
                '08_00': 0,
                '12_00': 0,
                '16_00': 0,
                '20_00': 0,
                MAX: 0,
                TEMP_DATE: this.props.group2Date
              }],
              noGroup2Results: true
            })
          }
          this.setState({
            group2Loaded: true, 
            results: [this.state.group1Results, this.state.group2Results]
          })
        }); 
      }
    )
  }
  
  render() {
    var {group1Loaded, group2Loaded, results } = this.state;
    if (!group1Loaded || !group2Loaded) {
      return <div className="CompareCowLoading">Loading... Cow Temperature Data</div>;
    } else if(this.state.results.length === 0) {
        return (
        <div id="CompareCowTempDayWrapper"> 
          <span class="CompareCowNoData">No Temperature data</span>
        </div>
     )
    }
    else{
      return (
        <div id="CompareCowTempDayWrapper"> 
          <div >
            <h1 className="cow-temp-heading">Temp - DEG C</h1>
            <BarChart width={700} height={500} data={[
                {'name': 'Max', 'group1': results[0][0].MAX, 'group2': results[1][0].MAX,},
                {'name': '00_00', 'group1': results[0][0]["00_00"], 'group2': results[1][0]["00_00"],},
                {'name': '04_00', 'group1': results[0][0]["04_00"], 'group2': results[1][0]["04_00"],},
                {'name': '08_00', 'group1': results[0][0]["08_00"], 'group2': results[1][0]["08_00"],},
                {'name': '12_00', 'group1': results[0][0]["12_00"], 'group2': results[1][0]["12_00"],},
                {'name': '16_00', 'group1': results[0][0]["16_00"], 'group2': results[1][0]["16_00"],},
                {'name': '20_00', 'group1': results[0][0]["20_00"], 'group2': results[1][0]["20_00"],},
              ]} 
                margin={{top: 20, right: 30, left: 10, bottom: 10}}>
                
            < CartesianGrid strokeDasharray="3 3"/>
              <XAxis dataKey="name"/>
              <YAxis/>
              <Tooltip/>
              <Legend />
            <Bar dataKey="group1" fill="#FF0000" />
            <Bar dataKey="group2" fill="#0606DE" />
            </BarChart> 
          </div> 
        </div>  
      );
    }
  }
}
export default CompareCowTempDay;


