import React, { Component } from 'react';
import CompareCowTempRange from '../CompareCowTempRange/CompareCowTempRange';
import CowDetails from '../../CowDetails/CowDetails';
import CompareCowActivityRange from '../CompareCowActivityRange/CompareCowActivityRange';
import CompareCowMapRange from '../CompareCowMapRange/CompareCowMapRange';
import './CompareCowRangeDisplay.css';

class CompareCowRangeDisplay extends Component {
    constructor(props){
        super(props)
        this.state = {
            cowIdGroup1: null,
            cowIdGroup2: null,
            group1StartDate: null,
            group2StartDate: null,
            amountOfDays: null
        }
    }

    componentDidMount(){
        const { cowIdGroup1 } = this.props.location.state
        const { cowIdGroup2 } = this.props.location.state
        const { group1StartDate } = this.props.location.state
        const { group2StartDate } = this.props.location.state
        const { amountOfDays } = this.props.location.state
        this.setState({
            cowIdGroup1: cowIdGroup1,
            cowIdGroup2: cowIdGroup2,
            group1StartDate: group1StartDate,
            group2StartDate: group2StartDate,
            amountOfDays: amountOfDays
        })
    }

    render() {
        if(this.state.cowIdGroup1 && this.state.cowIdGroup2 ){
          return (
            <div className="CompareCowRangeDisplay">
              <h1 className="CompareCowSearchTitle">Look Up Cow - Date Range</h1>
                <CompareCowActivityRange
                    cowIdGroup1={this.state.cowIdGroup1}
                    cowIdGroup2={this.state.cowIdGroup2}
                    group1Date={this.state.group1StartDate}
                    group2Date={this.state.group2StartDate}
                    amountOfDays={this.state.amountOfDays}
                />
              <div className="CompareCowDayDisplayWrapper">
                <div className="CompareCowRangeDisplayGraphWrapper">
                  <CompareCowTempRange                         
                      cowIdGroup1={this.state.cowIdGroup1}
                      cowIdGroup2={this.state.cowIdGroup2}
                      group1Date={this.state.group1StartDate}
                      group2Date={this.state.group2StartDate}
                      amountOfDays={this.state.amountOfDays}
                  />
                  <CompareCowMapRange 
                      cowIdGroup1={this.state.cowIdGroup1}
                      cowIdGroup2={this.state.cowIdGroup2}
                      group1Date={this.state.group1StartDate}
                      group2Date={this.state.group2StartDate}
                      amountOfDays={this.state.amountOfDays}
                  />
                </div>
                <div className="CompareCowRangeDetailsWrapper">
                  <div>
                    <h2 className="CompareCowGroupTitle">Group 1 Details:</h2>
                    <CowDetails items={this.state.cowIdGroup1} />
                  </div>
                    <div>
                    <h2 className="CompareCowGroupTitle">Group 2 Details:</h2>
                    <CowDetails items={this.state.cowIdGroup2} />
                  </div>
                </div>
              </div>
          </div>
            )
        } else {
            return (
                <div className="CompareCowRangeDisplay">
                    <span className="CompareCowLoading">Loading...</span>
                </div>
            )
        }
    }
}

export default CompareCowRangeDisplay