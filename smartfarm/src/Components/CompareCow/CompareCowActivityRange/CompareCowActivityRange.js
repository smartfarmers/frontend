
import React from "react";
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';
import './CompareCowActivityRange.css';

class CompareCowActivityRange extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      group1Loaded: false,
      group2Loaded: false,
      allDataLoaded: false,
      cowIdGroup1: [],
      cowIdGroup2: [],
      group1Results: [],
      group2Results: [],
      combinedResults: [],
      amountOfDays: 0,
    }
  }

  componentDidMount(){
    this.setState({
      cowIdGroup1: this.props.cowIdGroup1,
      cowIdGroup2: this.props.cowIdGroup2,
      group1Date: this.props.group1Date,
      group2Date: this.props.group2Date,
      amountOfDays: this.props.amountOfDays
    }, async () => {
      await this.getActRangeData(this.state.cowIdGroup1, 
        this.state.cowIdGroup2,  
        this.state.group1Date, 
        this.state.group2Date, 
        this.state.amountOfDays
      );
    })
  }

  async getActRangeData(cowIdGroup1, cowIdGroup2,  group1Date, group2Date, amountOfDays){
    for(let dayCounter = 0; dayCounter < amountOfDays; dayCounter++){
      let dayNumber = dayCounter + 1
      let day = new Date();
      let addValue = 1;
      if( dayCounter == 0 ){
        addValue = 0
      }
      day = group1Date.setDate(group1Date.getDate() + addValue);
      day = new Date(day);
      day = this.formatDate(day)
      await cowIdGroup1.forEach( async (element, index) => {
        fetch(`${process.env.REACT_APP_API_URL}/actday/` + element + '/' + day, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
        .then(res => res.json())
        .then(json => {
          if(json.response !== null){
            if(json.response.length !== 0){
              if(index === 0){
                this.addToAverage(json.response, 1, true, dayNumber);
              }
              this.addToAverage(json.response, 1, false, dayNumber);
            }
            if(index === this.state.cowIdGroup1.length - 1){
              this.averageData(1);
          }
          }
        }).then( () => {
          if(amountOfDays - 1 === dayCounter){
            this.setState({
              group1Loaded: true
            }) 
          }
        })
      })
      addValue++
    }

    for(let dayCounter = 0; dayCounter < amountOfDays; dayCounter++){
      let dayNumber = dayCounter + 1
      let day = new Date();
      let addValue = 1;
      if( dayCounter == 0 ){
        addValue = 0
      }
      day = group2Date.setDate(group2Date.getDate() + addValue);
      day = new Date(day);
      day = this.formatDate(day)
      await cowIdGroup2.forEach( (element, index) => {
        fetch(`${process.env.REACT_APP_API_URL}actday/` + element + '/' + day, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
        .then(res => res.json())
        .then(json => {
          if(json.response !== null){
            if(json.response.length !== 0){
              if(index === 0){
                this.addToAverage(json.response, 2, true, dayNumber);
              }
              this.addToAverage(json.response, 2, false, dayNumber);
            }
            if(index === this.state.cowIdGroup2.length - 1){
                this.averageData(2);
            }
          }
        }).then(() => {
          if(amountOfDays - 1 === dayCounter){
            this.joinGroups();
            this.setState({
              group2Loaded: true
            }) 
          }
        })
      })
    }
  }

  formatDate(date){
    let formattedDate = date.toISOString()
    formattedDate = formattedDate.split("T")[0]
    return formattedDate
  }

  addToAverage(response, groupNum, firstResult, dayCounter){
    let res = this.parseActData(response, groupNum, dayCounter);
    if(groupNum === 1){
      res.forEach((element, index)=> {
        if(firstResult){
          this.setState({
            group1Results: [...this.state.group1Results, {
              dateTime: element["dateTime"],
              restingGroup1: element["restingGroup1"],
              walkingGroup1: element["walkingGroup1"],
              grazingGroup1: element["grazingGroup1"]
            }]
          })
        } else {
          var newResults = this.state.group1Results;
          newResults[index].restingGroup1 += element["restingGroup1"];
          newResults[index].walkingGroup1 += element["walkingGroup1"];
          newResults[index].grazingGroup1 += element["grazingGroup1"];
          this.setState({
            group1Results: newResults
          })
        }
      })
    } else {
      res.forEach((element, index)=> {
        if(firstResult){
          this.setState({
            group2Results: [...this.state.group2Results, {
              dateTime: element["dateTime"],
              restingGroup2: element["restingGroup2"],
              walkingGroup2: element["walkingGroup2"],
              grazingGroup2: element["grazingGroup2"]
            }]
          })
        } else {
          var newResults = this.state.group2Results;
          newResults[index].restingGroup2 += element["restingGroup2"];
          newResults[index].walkingGroup2 += element["walkingGroup2"];
          newResults[index].grazingGroup2 += element["grazingGroup2"];
          this.setState({
            group2Results: newResults
          })
        }
      })
    }
  }

  averageData(group){
    var res;
    var groupCount;
    if(group === 1){
      res = this.state.group1Results;
      groupCount = this.state.cowIdGroup1.length;
      res.forEach( element => {
        element.grazingGroup1 /= groupCount;
        element.restingGroup1 /= groupCount;
        element.walkingGroup1 /= groupCount;
      })
      this.setState({
        group1Results: res
      })
    } else {
      res = this.state.group2Results;
      groupCount = this.state.cowIdGroup2.length;
      res.forEach( element => {
        element.grazingGroup2 /= groupCount;
        element.restingGroup2 /= groupCount;
        element.walkingGroup2 /= groupCount;
      })
      this.setState({
        group2Results: res
      })
    }
  }

  parseActData(response, group, dayCounter){
    let result = []
    response.forEach((element, index) => {
        let resting;
        let walking;
        let grazing;
        try{
            resting = element["hourSummaries"][0]["count"]
        } catch{
            resting = 0;
        }
        try{
            walking = element["hourSummaries"][1]["count"]
        } catch{
            walking = 0;
        }
        try{
            grazing = element["hourSummaries"][2]["count"]
        } catch{
            grazing = 0;
        }
        var dateTime;
        if(group === 1){
          dateTime = {
            "dateTime" : `Day ${dayCounter} - ${element["hour"]}`,
            "restingGroup1" : resting,
            "walkingGroup1" : walking,
            "grazingGroup1" : grazing
          }
        } else {
          dateTime = {
            "dateTime" : `Day ${dayCounter} - ${element["hour"]}`,
            "restingGroup2" : resting,
            "walkingGroup2" : walking,
            "grazingGroup2" : grazing
          }
        }
        result.push(dateTime)
    });
    return result;
  }

  joinGroups(){
    console.log("Combining results");
    var result = [];
    var group2Res = this.state.group2Results;
    this.state.group1Results.forEach( (element, index) => {
      if(group2Res[index] === null){
        result.push({
          dateTime: element.dateTime,
          grazingGroup1: element.grazingGroup1,
          restingGroup1: element.restingGroup1,
          walkingGroup1: element.walkingGroup1,
          restingGroup2: 0,
          walkingGroup2: 0,
          grazingGroup2: 0,
        })
      } else {
        result.push({
          dateTime: element.dateTime,
          grazingGroup1: element.grazingGroup1,
          restingGroup1: element.restingGroup1,
          walkingGroup1: element.walkingGroup1,
          restingGroup2: (group2Res[index].restingGroup2 ? group2Res[index].restingGroup2 : 0),
          walkingGroup2: (group2Res[index].walkingGroup2 ? group2Res[index].walkingGroup2 : 0),
          grazingGroup2: (group2Res[index].grazingGroup2 ? group2Res[index].grazingGroup2 : 0),
        })
      }
    })
    this.setState({
      combinedResults: result,
      allDataLoaded: true
    })
  }
  
  render() {
    var { allDataLoaded, group1Results, group2Results } = this.state;
    if (!allDataLoaded) {
      return <div className="CompareCowLoading">Loading... Activity Data</div>;
    } else if(group1Results.length === 0 || group2Results.length === 0){
      return (
        <div id="CompareCowActivityRangeWrapper"> 
          <span class="CompareCowNoData">No Activity data</span>
        </div>
     )
    } else{
      let parsedResponse = this.state.combinedResults
      return (
        <div id="CompareCowActivityRangeWrapper">
            <h1>Cow Activity</h1>
            <ResponsiveContainer width="100%" height={300}>
            <LineChart data={parsedResponse}
                    margin={{top: 5, right: 30, left: 20, bottom: 5}}>
            < CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="dateTime"/>
                <YAxis/>
                <Tooltip/>
                <Legend />
                <Line dataKey="restingGroup1" stroke="#1E90FF" />
                <Line dataKey="grazingGroup1" stroke="#228B22" />
                <Line dataKey="walkingGroup1" stroke="#FF00FF" />
                <Line dataKey="restingGroup2" stroke="#000000" />
                <Line dataKey="grazingGroup2" stroke="#DE0606" />
                <Line dataKey="walkingGroup2" stroke="#06DE72" />
            </LineChart> 
            </ResponsiveContainer>
            </div>
      );
    }
  }
}
export default CompareCowActivityRange;
