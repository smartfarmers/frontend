import React from "react";
import './CompareCowBattDay.css';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,} from 'recharts';

class CompareCowBattDay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      battDay: [],
      isLoaded: false,
      cowId: '',
      date:''
    }
  }

  // fetches battery with cow_id for a specified date
  componentDidMount(){
    fetch(`${process.env.REACT_APP_API_URL}/battdate/` + this.props.cowId + '/'+ this.props.date, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
      .then(res => res.json())
      .then(json => {
       this.setState({
         isLoaded: true,
         battDay: json, 
       }) 
    }); 
  }
  
  render() {
    var {isLoaded, battDay } = this.state;
    if (!isLoaded) {
      return <div>Loading...cowbattday</div>;
    } else if(battDay.response.length == 0){
     return <div id="cowBattDay-wrapper">
        <h1 className="cow-batt-heading">No Batt data </h1>
              <BarChart width={450} height={270} data={[
                          {'name': '00_00', 'reading': 0,},
                          {'name': '04_00', 'reading': 0,},
                          {'name': '08_00', 'reading': 0,},
                          {'name': '12_00', 'reading': 0,},
                          {'name': '16_00', 'reading': 0,},
                          {'name': '20_00', 'reading': 0,},
                        ]}
                        margin={{top: 40, right: 20, left: 10, bottom: 0}}>
              < CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="name"/>
                <YAxis/>
                <Tooltip/>
                <Legend />
                <Bar dataKey="no data" fill="#FFFFFF" />
              </BarChart> 
        
        </div>

    }
    else{
      return (
        <div id="cowBattDay-wrapper">
          {battDay.response.map(item => ( 
            <div key={item.id}>
              <h1 className="cow-batt-heading">Batt Volt - mV</h1>
              <BarChart width={450} height={270} data={[
                          {'name': '00_00', 'reading': item.BATT_00_00,},
                          {'name': '04_00', 'reading': item.BATT_04_00,},
                          {'name': '08_00', 'reading': item.BATT_08_00,},
                          {'name': '12_00', 'reading': item.BATT_12_00,},
                          {'name': '16_00', 'reading': item.BATT_16_00,},
                          {'name': '20_00', 'reading': item.BATT_20_00,},
                        ]}
                        margin={{top: 40, right: 20, left: 10, bottom: 0}}>
              < CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="name"/>
                <YAxis/>
                <Tooltip/>
                <Legend />
                <Bar dataKey="reading" fill="#006400" />
              </BarChart> 
            </div> 
            ))}
        </div>  
      );
    }
  }
}
export default CompareCowBattDay;


