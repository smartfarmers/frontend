
import React from "react";
import './CompareCowTempRange.css';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';

class CompareCowTempRange extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      group1Loaded: false,
      group2Loaded: false,
      allDataLoaded: false,
      cowIdGroup1: [],
      cowIdGroup2: [],
      group1Results: [],
      group2Results: [],
      combinedResults: [],
      amountOfDays: 0,
    }
  }

  // fetches temperature with cow_id for a specified date
  componentDidMount(){
    this.setState({
      cowIdGroup1: this.props.cowIdGroup1,
      cowIdGroup2: this.props.cowIdGroup2,
      group1Date: this.props.group1Date,
      group2Date: this.props.group2Date,
      amountOfDays: this.props.amountOfDays
    }, async () => {
      let group1StartDate = new Date(this.state.group1Date);
      let group1EndDate = new Date(this.state.group1Date)
      group1EndDate.setDate(group1StartDate.getDate() + parseInt(this.state.amountOfDays));
      group1EndDate = this.formatDate(group1EndDate);
      
      let request = { 
        cowGroup: this.state.cowIdGroup1
      }

      await fetch((`${process.env.REACT_APP_API_URL}/grouptemprange/` + this.formatDate(this.state.group1Date) + '/' + group1EndDate), {
        method: 'post',
        headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
        body: JSON.stringify(request)
      }) 
        .then(res => res.json())
        .then(json => {
          if(json.response.length !== 0){
            let group1 = this.parseTempData(json.response, 1)
              this.setState({
                  group1Results: group1
              })
          }
          this.setState({
              group1Loaded: true,
          })
      });

      let group2StartDate = new Date(this.state.group2Date);
      let group2EndDate = new Date(this.state.group2Date)
      group2EndDate.setDate(group2StartDate.getDate() + parseInt(this.state.amountOfDays));
      group2EndDate = this.formatDate(group2EndDate);
      
      request = { 
        cowGroup: this.state.cowIdGroup2
      }

      await fetch((`${process.env.REACT_APP_API_URL}/grouptemprange/` + this.formatDate(this.state.group2Date) + '/' + group2EndDate), {
        method: 'post',
        headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
        body: JSON.stringify(request)
      }) 
        .then(res => res.json())
        .then(json => {
          if(json.response.length !== 0){
            let group2 = this.parseTempData(json.response, 2)
            this.setState({
                  group2Results: group2
            })
          }
          this.joinGroups()
          this.setState({
              group2Loaded: true,
          })
      });
    })
  }
  
  formatDate(date){
    let formattedDate = date.toISOString()
    formattedDate = formattedDate.split("T")[0]
    return formattedDate
  }

  parseTempData(tempData, group){
    let wrapperArray = [];
    if(group === 1){
      tempData.forEach((element, index) => {
        let MIN;
        let MAX;
        let AVG;
        try{
          MAX = element["MAX"]
        } catch{
          MAX = 0;
        }
        try{
          MIN = element["MIN"]
        } catch{
          MIN = 0;
        }
        try{
          AVG = element["AVG"]
        } catch{
          AVG = 0;
        }
  
        let result = {
          "dateTime" : `Day ${index + 1}`,
          "Group1MIN" : MIN,
          "Group1MAX" : MAX,
          "Group1AVG" : AVG
        }
        wrapperArray.push(result)
      });
    }

    if(group === 2){
      wrapperArray = [];
      tempData.forEach((element, index) => {
        let MIN;
        let MAX;
        let AVG;
        try{
          MAX = element["MAX"]
        } catch{
          MAX = 0;
        }
        try{
          MIN = element["MIN"]
        } catch{
          MIN = 0;
        }
        try{
          AVG = element["AVG"]
        } catch{
          AVG = 0;
        }
  
        let result = {
          "dateTime" : `Day ${index + 1}`,
          "Group2MIN" : MIN,
          "Group2MAX" : MAX,
          "Group2AVG" : AVG
        }
        wrapperArray.push(result)
      });
    }
    return wrapperArray
  }

  joinGroups(){
    var result = [];
    var group2Res = this.state.group2Results;
    this.state.group1Results.forEach( (element, index) => {
      if(group2Res[index] === null){
        result.push({
          dateTime: element.dateTime,
          Group1MIN: element.Group1MIN,
          Group1MAX: element.Group1MAX,
          Group1AVG: element.Group1AVG,
          Group2MIN: 0,
          Group2MAX: 0,
          Group2AVG: 0,
        })
      } else {
        result.push({
          dateTime: element.dateTime,
          Group1MIN: element.Group1MIN,
          Group1MAX: element.Group1MAX,
          Group1AVG: element.Group1AVG,
          Group2MIN: (group2Res[index].Group2MIN ? group2Res[index].Group2MIN : 0),
          Group2MAX: (group2Res[index].Group2MAX ? group2Res[index].Group2MAX : 0),
          Group2AVG: (group2Res[index].Group2AVG ? group2Res[index].Group2AVG : 0),
        })
      }
    })
    this.setState({
      combinedResults: result,
      allDataLoaded: true
    })
  }
  

  render() {
    var {group1Loaded, group2Loaded } = this.state;
    if (!group2Loaded || !group1Loaded) {
      return <div className="CompareCowLoading">Loading... Temperature data</div>;
    } else if(this.state.combinedResults.length === 0){
      return (
        <div id="CompareCowTempDayWrapper"> 
          <span class="CompareCowNoData">No temperature data</span>
        </div>
     )
    } else{
      return (
        <div id="CompareCowTempRangeWrapper">
          <h1 className="CowTempRangeHeading">Temp - DEG C</h1>
          <ResponsiveContainer width="100%" height={370}>
          <LineChart data={this.state.combinedResults}
                  margin={{top: 5, right: 30, left: 20, bottom: 5}}>
          < CartesianGrid strokeDasharray="3 3"/>
              <XAxis dataKey="dateTime"/>
              <YAxis/>
              <Tooltip/>
              <Legend />
              <Line dataKey="Group1MAX" stroke="#FF0000" />
              <Line dataKey="Group1MIN" stroke="#228B22" />
              <Line dataKey="Group1AVG" stroke="#1E90FF" />
              <Line dataKey="Group2MAX" stroke="#FF0000" />
              <Line dataKey="Group2MIN" stroke="#228B22" />
              <Line dataKey="Group2AVG" stroke="#1E90FF" />
              
          </LineChart> 
          </ResponsiveContainer>
        </div>  
      );
    }
  }
}
export default CompareCowTempRange;


