import React, { Component } from 'react';
import { ReactBingmaps } from 'react-bingmaps';
import './CompareCowMap.css';

class CompareCowMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
          group1Loaded: false,
          group2Loaded: false,
          group1Results: [],
          group2Results: [],
          combinedResults: [],
        }
      }

      handlePinOnHover(event){
      }

    parseCowLocData(res, group){
        // Make the mouse over event for the pins display the date time of the pin and cow id
        let result = []
        res.forEach( response => {
            if(group === 1){
                response.forEach(element => {
                    if(element["loc_lat"] != null || element["loc_lon"] != null){
                        let pin = {
                            "location" : [
                                element["loc_lat"],
                                element["loc_lon"]
                            ],
                            "pushPinOption":{ title: 'Pushpin Title', description: 'Pushpin' },
                            "option" : {
                                "color" : "red"
                            },
                            "addHandler" : {
                                "type" : "mouseover",
                                callback : this.handlePinOnHover()
                            }
                        };
                        result.push(pin);
                    }
                });
            } else {
                response.forEach(element => {
                    if(element["loc_lat"] != null || element["loc_lon"] != null){
                        let pin = {
                            "location" : [
                                element["loc_lat"],
                                element["loc_lon"]
                            ],
                            "option" : {
                                "color" : "blue"
                            },
                            "addHandler" : {
                                "type" : "mouseover",
                                callback : this.handlePinOnHover()
                            }
                        };
                        result.push(pin);
                    }
                });
            }
        })
        return result;
    }
      

    // fetches location for day with cow_id for a specified date
    componentDidMount(){
        this.setState({
            cowIdGroup1: this.props.cowIdGroup1,
            cowIdGroup2: this.props.cowIdGroup2,
            group1Date: this.props.group1Date,
            group2Date: this.props.group2Date
            }, () => {
                let request = { 
                cowGroup: this.state.cowIdGroup1
                }
                fetch((`${process.env.REACT_APP_API_URL}/grouplocday/` + this.props.group1Date), {
                    method: 'post',
                    headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
                    body: JSON.stringify(request)
                }) 
                .then(res => res.json())
                .then(json => {
                    if(json.response.length !== 0){
                        this.setState({
                            group1Results: this.parseCowLocData(json.response, 1)
                        })
                    }
                    this.setState({
                        group1Loaded: true,
                    })
                });

                request = { 
                    cowGroup: this.state.cowIdGroup1
                }
                fetch((`${process.env.REACT_APP_API_URL}/grouplocday/` + this.props.group2Date), {
                    method: 'post',
                    headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
                    body: JSON.stringify(request)
                }) 
                .then(res => res.json())
                .then(json => {
                    console.log(json)
                    if(json.response.length !== 0){
                        this.setState({
                            group2Results: this.parseCowLocData(json.response, 2)
                        })
                    }
                    this.setState({
                        group2Loaded: true,
                        combinedResults: this.state.group1Results.concat(this.state.group2Results)
                    })
                });
            }
        )
    }

    render() {
        var {group1Loaded, group2Loaded, combinedResults } = this.state;
        if (!group2Loaded || !group1Loaded) {
          return <div className="CompareCowLoading">Loading...Cow Location Data</div>;
        } else if(this.state.combinedResults.length === 0) {
            return <div id="CompareCowMap">
            <span class="CompareCowNoData">No Location data </span>
         </div>
        } else {
            return (
                <div className="CompareCowMap">
                    <h1 className="cow-map-heading">Cow Map</h1>
                    <ReactBingmaps 
                        bingmapKey = "Al4i0oHFLW-z-hDROXL89Wp37vGOgqMJT6aX6ompovuUH9Ef54yzbBuGdyWhRtEe"
                        center = {[-35.0479, 147.312]}
                        mapTypeId= {"aerial"}
                        navigationBarMode={"minified"}
                        pushPins={combinedResults}
                    />
                </div>
            )
        }
    }
}

export default CompareCowMap