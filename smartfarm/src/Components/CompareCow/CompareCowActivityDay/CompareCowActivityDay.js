
import React from "react";
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';
import './CompareCowActivityDay.css';

class CompareCowActivityDay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      group1Loaded: false,
      group2Loaded: false,
      cowIdGroup1: [],
      cowIdGroup2: [],
      group1Results: [],
      group2Results: [],
      combinedResults: [],
    }
  }

  // fetches temperature with cow_id for a specified date
  componentDidMount(){
    this.setState({
      cowIdGroup1: this.props.cowIdGroup1,
      cowIdGroup2: this.props.cowIdGroup2,
      group1Date: this.props.group1Date,
      group2Date: this.props.group2Date
    }, () => {
      this.state.cowIdGroup1.forEach( async (element, index) => {
        await fetch(`${process.env.REACT_APP_API_URL}/actday/` + element + '/' + this.props.group1Date, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
        .then(res => res.json())
        .then(json => {
          if(json.response !== null){
            if(index === 0){
              this.addToAverage(json.response, 1, true);
            } else {
              this.addToAverage(json.response, 1, false);
            }
            if(index === this.state.cowIdGroup1.length - 1){
              this.averageData(1)
              this.setState({
                group1Loaded: true
              })
            } 
          } else {
            this.setState({
              group1Loaded: true
            })
          }
        });
      })

      this.state.cowIdGroup2.forEach( async (element, index) => {
        await fetch(`${process.env.REACT_APP_API_URL}/actday/` + element + '/' + this.props.group2Date, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
        .then(res => res.json())
        .then(json => {
          if( json.response !== null){
            if(index === 0){
              this.addToAverage(json.response, 2, true);
            } else {
              this.addToAverage(json.response, 2, false);
            }
            if(index === this.state.cowIdGroup2.length - 1){
              this.averageData(2);
              this.joinGroups();
              this.setState({
                group2Loaded: true
              })
            }
          } else {
            this.setState({
              group2Loaded: true
            })
          }
        });
      })
    })
  }

  addToAverage(response, groupNum, firstResult){
    let res = this.parseActData(response, groupNum);
    if(groupNum === 1){
      res.forEach((element, index)=> {
        if(firstResult){
          this.setState({
            group1Results: [...this.state.group1Results, {
              dateTime: element["dateTime"],
              restingGroup1: element["restingGroup1"],
              walkingGroup1: element["walkingGroup1"],
              grazingGroup1: element["grazingGroup1"]
            }]
          })
        } else {
          var newResults = this.state.group1Results;
          newResults[index].restingGroup1 += element["restingGroup1"];
          newResults[index].walkingGroup1 += element["walkingGroup1"];
          newResults[index].grazingGroup1 += element["grazingGroup1"];
          this.setState({
            group1Results: newResults
          })
        }
      })
    } else {
      res.forEach((element, index)=> {
        if(firstResult){
          this.setState({
            group2Results: [...this.state.group2Results, {
              dateTime: element["dateTime"],
              restingGroup2: element["restingGroup2"],
              walkingGroup2: element["walkingGroup2"],
              grazingGroup2: element["grazingGroup2"]
            }]
          })
        } else {
          var newResults = this.state.group2Results;
          newResults[index].restingGroup2 += element["restingGroup2"];
          newResults[index].walkingGroup2 += element["walkingGroup2"];
          newResults[index].grazingGroup2 += element["grazingGroup2"];
          this.setState({
            group2Results: newResults
          })
        }
      })
    }
  }

  averageData(group){
    var res;
    var groupCount;
    if(group === 1){
      res = this.state.group1Results;
      groupCount = this.state.cowIdGroup1.length;
      res.forEach( element => {
        element.grazingGroup1 /= groupCount;
        element.restingGroup1 /= groupCount;
        element.walkingGroup1 /= groupCount;
      })
      this.setState({
        group1Results: res
      })
    } else {
      res = this.state.group2Results;
      groupCount = this.state.cowIdGroup2.length;
      res.forEach( element => {
        element.grazingGroup2 /= groupCount;
        element.restingGroup2 /= groupCount;
        element.walkingGroup2 /= groupCount;
      })
      this.setState({
        group2Results: res
      })
    }
  }

  parseActData(response, group){
    let result = []
    response.forEach(element => {
        let resting;
        let walking;
        let grazing;
        try{
            resting = element["hourSummaries"].filter( (item) => {
              return item.act_stat === 1;
            })[0].count
        } catch{
            resting = 0;
        }
        try{
            walking = element["hourSummaries"].filter( (item) => {
              return item.act_stat === 2;
            })[0].count
        } catch{
            walking = 0;
        }
        try{
          grazing = element["hourSummaries"].filter( (item) => {
            return item.act_stat === 3;
          })[0].count
        } catch{
            grazing = 0;
        }
        var dateTime;
        if(group === 1){
          dateTime = {
            "dateTime" : `${element["hour"]}`,
            "restingGroup1" : resting,
            "walkingGroup1" : walking,
            "grazingGroup1" : grazing
          }
        } else {
          dateTime = {
            "dateTime" : `${element["hour"]}`,
            "restingGroup2" : resting,
            "walkingGroup2" : walking,
            "grazingGroup2" : grazing
          }
        }
        result.push(dateTime)
    });
    return result;
  }

  joinGroups(){
    var result = [];
    var group2Res = this.state.group2Results;
    if(this.state.group1Results.length !== 0 && group2Res.length !== 0){
      this.state.group1Results.forEach( (element, index) => {
        result.push({
          dateTime: element.dateTime,
          grazingGroup1: element.grazingGroup1,
          restingGroup1: element.restingGroup1,
          walkingGroup1: element.walkingGroup1,
          restingGroup2: group2Res[index].restingGroup2,
          walkingGroup2: group2Res[index].walkingGroup2,
          grazingGroup2: group2Res[index].grazingGroup2,
        })
      })
    }
    this.setState({
      combinedResults: result
    })
  }
  
  render() {
    var {group1Loaded, group2Loaded } = this.state;
    if (!group1Loaded || !group2Loaded) {
      return <div className="CompareCowLoading">Loading... Cow Activity Data</div>;
    } else if(this.state.combinedResults.length === 0){
      return (
        <div id="CompareCowActivityDayWrapper">
          <span class="CompareCowNoData">No Activity data for one or both of the groups of cows</span>
        </div>
      );
    } else{
      return (
        <div id="CompareCowActivityDayWrapper">
            <h1>Cow Activity</h1>
            <ResponsiveContainer width="100%" height={300}>
            <LineChart data={this.state.combinedResults}
                    margin={{top: 2, right: 30, left: 5, bottom: 20}}>
            < CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="dateTime"/>
                <YAxis/>
                <Tooltip/>
                <Legend />
                <Line dataKey="restingGroup1" stroke="#1E90FF" />
                <Line dataKey="grazingGroup1" stroke="#228B22" />
                <Line dataKey="walkingGroup1" stroke="#FF00FF" />
                <Line dataKey="restingGroup2" stroke="#000000" />
                <Line dataKey="grazingGroup2" stroke="#DE0606" />
                <Line dataKey="walkingGroup2" stroke="#06DE72" />
            </LineChart> 
            </ResponsiveContainer>
            </div>
      );
    }
  }
}
export default CompareCowActivityDay;
