import React, { Component } from 'react';
import './Table.css';

class Table extends Component {
 
  renderHeader() {
    return this.props.headers.map(header => {
      return <th>{header}</th>
    });
  }

  renderTableData() {
    return this.props.tableData.map((data, i) => {
      return (
        <tr>
          {this.props.tableData[i].map(data => {
            return (
              <td>
                {data}
              </td>
            );
          })}
        </tr>
      );
    })
  }

  render() {
    return (
      <table>
        <thead>
          <tr>
            {this.renderHeader()}
          </tr>
        </thead>

        <tbody>
            {this.renderTableData()} 
        </tbody>

      </table>
    );
  }
}

export default Table;