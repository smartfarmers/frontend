import React, { Component } from 'react';
import LookUpHerdTempRange from '../LookUpHerdTempRange/LookUpHerdTempRange';
import CowDetails from '../../CowDetails/CowDetails';
import LookUpHerdActivityRange from '../LookUpHerdActivityRange/LookUpHerdActivityRange';
import LookUpHerdMapRange from '../LookUpHerdMapRange/LookUpHerdMapRange';
import './LookUpHerdRangeDisplay.css';

class LookUpHerdRangeDisplay extends Component {
    constructor(props){
        super(props)
        this.state = {
            cowIdGroup: null,
            groupStartDate: null,
            groupEndDate: null,
        }
    }

    componentDidMount(){
        const { cowIdGroup } = this.props.location.state
        const { groupStartDate } = this.props.location.state
        const { groupEndDate } = this.props.location.state
        this.setState({
            cowIdGroup: cowIdGroup,
            groupStartDate: groupStartDate,
            groupEndDate: groupEndDate,
        })
    }

    render() {
        if(this.state.cowIdGroup){
            return (
                <div className="LookupHerdRangeDisplay">
                    <h1 className="LookUpHerdSearchTitle">Look Up Herd - Date Range</h1>
                    <LookUpHerdActivityRange
                        cowIdGroup={this.state.cowIdGroup}
                        groupStartDate={this.state.groupStartDate}
                        groupEndDate={this.state.groupEndDate}
                    />
                    <div className="LookUpHerdRangeDisplayGraphWrapper">
                      <LookUpHerdTempRange
                          cowIdGroup={this.state.cowIdGroup}
                          groupStartDate={this.state.groupStartDate}
                          groupEndDate={this.state.groupEndDate}
                      />
                      <LookUpHerdMapRange 
                          cowIdGroup={this.state.cowIdGroup}
                          groupStartDate={this.state.groupStartDate}
                          groupEndDate={this.state.groupEndDate}
                      />
                    </div>
                    <h2 className="LookUpHerdGroupTitle">Group Details:</h2>
                    <CowDetails items={this.state.cowIdGroup} />
                </div>
            )
        } else {
            return (
                <div className="LookupHerdRangeDisplay">
                    <h1 className="LookUpHerdLoading">Loading...</h1>
                </div>
            )
        }
    }
}

export default LookUpHerdRangeDisplay