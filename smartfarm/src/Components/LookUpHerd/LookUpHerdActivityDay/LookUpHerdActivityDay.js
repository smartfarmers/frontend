
import React from "react";
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';
import './LookUpHerdActivityDay.css';

class LookUpHerdActivityDay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      groupLoaded: false,
      cowIdGroup: [],
      groupResults: [],
    }
  }

  // fetches temperature with cow_id for a specified date
  componentDidMount(){
    this.setState({
      cowIdGroup: this.props.cowIdGroup,
      startDate: this.props.startDate,
    }, () => {
      this.state.cowIdGroup.forEach( (element, index) => {
        fetch(`${process.env.REACT_APP_API_URL}/actday/` + element + '/' + this.props.startDate, 
          {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
        .then(res => res.json())
        .then(json => {
          if(json.response !== null){
            if(index === 0){
              this.addToAverage(json.response, 1, true);
            }
            this.addToAverage(json.response, 1, false);
            if(index === this.state.cowIdGroup.length - 1){
              this.averageData(1)
              this.setState({
                groupLoaded: true
              })
            } 
          } else {
            this.setState({
              groupLoaded: true
            })
          }
        });
      })
    })
  }

  addToAverage(response, firstResult){
    let res = this.parseActData(response);
      res.forEach((element, index)=> {
      if(firstResult){
        this.setState({
          groupResults: [...this.state.groupResults, {
            dateTime: element["dateTime"],
            resting: element["resting"],
            walking: element["walking"],
            grazing: element["grazing"]
          }]
        })
      } else {
        var newResults = this.state.groupResults;
        newResults[index].resting += element["resting"];
        newResults[index].walking += element["walking"];
        newResults[index].grazing += element["grazing"];
        this.setState({
          groupResults: newResults
        })
      }
    })
  }

  averageData(){
    var res;
    var groupCount;
    res = this.state.groupResults;
    groupCount = this.state.cowIdGroup.length;
    res.forEach( element => {
      element.grazing /= groupCount;
      element.resting /= groupCount;
      element.walking /= groupCount;
    })
    this.setState({
      groupResults: res
    })
  }

  parseActData(response){
    let result = []
    response.forEach(element => {
        let resting;
        let walking;
        let grazing;
        try{
            resting = element["hourSummaries"][0]["count"]
        } catch{
            resting = 0;
        }
        try{
            walking = element["hourSummaries"][1]["count"]
        } catch{
            walking = 0;
        }
        try{
            grazing = element["hourSummaries"][2]["count"]
        } catch{
            grazing = 0;
        }
        var dateTime;
          dateTime = {
            "dateTime" : `${element["day"]} - ${element["hour"]}`,
            "resting" : resting,
            "walking" : walking,
            "grazing" : grazing
          }
        result.push(dateTime)
    });
    return result;
  }
  
  render() {
    var {groupLoaded, groupResults} = this.state;
    if (!groupLoaded) {
      return <div className="LookUpHerdLoading">Loading... Cow Activity Data</div>;
    } else if(groupResults.length === 0){
      return (
        <div id="LookUpHerdActivityDayWrapper">
          <span class="LookUpHerdNoData">No Activity data for the herd</span>
        </div>
      );
    } else{
      return (
        <div id="LookUpHerdActivityDayWrapper">
            <h1>Cow Activity</h1>
            <ResponsiveContainer width="100%" height={300}>
            <LineChart data={this.state.groupResults}
                    margin={{top: 2, right: 30, left: 5, bottom: 20}}>
            < CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="dateTime"/>
                <YAxis/>
                <Tooltip/>
                <Legend />
                <Line dataKey="resting" stroke="#1E90FF" />
                <Line dataKey="grazing" stroke="#228B22" />
                <Line dataKey="walking" stroke="#FF00FF" />
            </LineChart> 
            </ResponsiveContainer>
            </div>
      );
    }
  }
}
export default LookUpHerdActivityDay;
