import React, { Component } from "react";
import { Link } from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "./LookUpHerdDateDetails.css";

class LookUpHerdSingleDate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cowIdGroup : [],
      groupStartDate : new Date(),
    };
  }

  componentDidMount(){
    this.setState({
      cowIdGroup: this.props.cowIdGroup,
    })
  }

  handleChange = date => {    
    this.setState({
      groupStartDate: date
    });
  };

  
  render() {
    return (
			<div className="LookUpHerdSingleDateDetails">
					<h1>Single Date Selector</h1>
					<label>Select Day:</label>
					<DatePicker
					className="startDatePicker"
					selected={this.state.groupStartDate}
					onChange={this.handleChange}
					/>
					<Link 
					to={{
							pathname: `/LookUpHerdDayDisplay`,
							state: {
							cowIdGroup: this.state.cowIdGroup,
							groupStartDate: this.state.groupStartDate,
							}
					}}
					>
					<button className="LookUpHerdRangeSubmit">Submit</button>
					</Link>
			</div>
    );
  }
}

export default LookUpHerdSingleDate;