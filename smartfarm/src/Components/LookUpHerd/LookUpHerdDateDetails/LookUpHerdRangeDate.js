import React, { Component } from "react";
import { Link } from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "./LookUpHerdDateDetails.css";

class LookUpHerdRangeDate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cowIdGroup : [],
      groupStartDate : new Date(),
      groupEndDate : new Date(),
    };
  }

  componentDidMount(){
    this.setState({
      cowIdGroup: this.props.cowIdGroup,
    })
  }

  handleChange = date => {    
    this.setState({
      groupStartDate: date
    });
  };

  handleChangeSecondDate = date => {    
    this.setState({
      groupEndDate: date
    });
  };
  
  render() {
    return (
        <div className="LookUpHerdRangeDateDetails">
            <h1>Date Range Selector</h1>
            <label>Start Date:</label>
            <DatePicker
            className="startDatePicker"
            selected={this.state.groupStartDate}
            onChange={this.handleChange}
            />
            <label>End Date:</label>
            <DatePicker
            className="endDatePicker"
            selected={this.state.groupEndDate}
            onChange={this.handleChangeSecondDate}
            />
            <Link 
            to={{
                pathname: `/LookUpHerdRangeDisplay`,
                state: {
                cowIdGroup: this.state.cowIdGroup,
                groupStartDate: this.state.groupStartDate,
                groupEndDate: this.state.groupEndDate,
                }
            }}
            >
            <button className="LookUpHerdRangeSubmit">Submit</button>
            </Link>
        </div>
    );
  }
}

export default LookUpHerdRangeDate;