import React, { Component } from "react";
import { Link } from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "./LookUpHerdDateDetails.css";

class LookUpHerdDateDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cowIdGroup : [],
      range : this.props.range,
      single : this.props.single,
      groupStartDate : new Date(),
      groupEndDate : new Date(),
      amountOfDays: 0
    };
  }

  componentDidMount(){
    this.setState({
      cowIdGroup: this.props.cowIdGroup,
    })
  }

  handleChange = date => {    
    this.setState({
      groupStartDate: date
    });
  };

  handleChangeSecondDate = date => {    
    this.setState({
      groupEndDate: date
    });
  };
  
  render() {
    if(this.state.range){
      return (
        <div className="DateDetails">
          <h1>Date Range Selector</h1>
          <label>Start Date for Group1: </label>
          <DatePicker
            className="startDatePicker"
            selected={this.state.groupStartDate}
            onChange={this.handleChange}
          />
          <label>End Date for Group2: </label>
          <DatePicker
            className="endDatePicker"
            selected={this.state.groupEndDate}
            onChange={this.handleChangeSecondDate}
          />
          <Link 
            to={{
              pathname: `/LookUpHerdRangeDisplay`,
              state: {
                cowIdGroup: this.state.cowIdGroup,
                groupStartDate: this.state.groupStartDate,
                groupEndDate: this.state.groupEndDate,
              }
            }}
          >
            <button>Submit</button>
          </Link>
        </div>
      );
    } else if (this.state.single) {
      return (
        <div className="DateDetails">
          <h1>Single Date Selector</h1>
          <label>Select Day:</label>
          <DatePicker
            className="startDatePicker"
            selected={this.state.groupStartDate}
            onChange={this.handleChange}
          />
          <Link 
            to={{
              pathname: `/LookUpHerdDayDisplay`,
              state: {
                cowIdGroup: this.state.cowIdGroup,
                groupStartDate: this.state.groupStartDate,
              }
            }}
          >
            <button>Submit</button>
          </Link>
        </div>
      );
    }
  }
}

export default LookUpHerdDateDetails;