
import React from "react";
import './LookUpHerdTempRange.css';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';

class LookUpHerdTempRange extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      groupLoaded: false,
      cowIdGroup: [],
      groupResults: [],
    }
  }

  // fetches temperature with cow_id for a specified date
  componentDidMount(){
    this.setState({
      cowIdGroup: this.props.cowIdGroup,
      groupStartDate: this.props.groupStartDate,
      groupEndDate: this.props.groupEndDate,
    }, async () => {
      let startDate = this.state.groupStartDate
      let endDate = this.state.groupEndDate
      startDate = this.formatDate(startDate)
      endDate = this.formatDate(endDate);
      
      let request = { 
        cowGroup: this.state.cowIdGroup
      }

      fetch((`${process.env.REACT_APP_API_URL}/grouptemprange/` + startDate + '/' + endDate), {
        method: 'post',
        headers: {            
          'Content-Type': 'application/json',
          "Authorization" : 'Bearer '+ localStorage.getItem('token')
        },
        body: JSON.stringify(request)
      }) 
        .then(res => res.json())
        .then(json => {
          if(json.response.length !== 0){
              this.setState({
                  groupResults: this.parseTempData(json.response)
              })
          }
          this.setState({
              groupLoaded: true,
          })
      });
    })
  }
  
  getDifferenceInDates(startDate, endDate){
    let diff = endDate.getTime() - startDate.getTime();
    let diffInDays = diff / (1000 * 3600 * 24);
    return parseInt(diffInDays)
  }

  formatDate(date){
    let formattedDate = date.toISOString()
    formattedDate = formattedDate.split("T")[0]
    return formattedDate
  }

  parseTempData(tempData){
    let wrapperArray = [];
    tempData.forEach(element => {
      let MIN;
      let MAX;
      let AVG;
      try{
        MAX = element["MAX"]
      } catch{
        MAX = 0;
      }
      try{
        MIN = element["MIN"]
      } catch{
        MIN = 0;
      }
      try{
        AVG = element["AVG"]
      } catch{
        AVG = 0;
      }

      let result = {
        "dateTime" : element["TEMP_DATE"],
        "MIN" : MIN,
        "MAX" : MAX,
        "AVG" : AVG
      }
      wrapperArray.push(result)
    });
    return wrapperArray
  }  

  render() {
    var {groupLoaded, groupResults } = this.state;
    if (!groupLoaded) {
      return <div className="LookUpHerdLoading">Loading... Temperature data</div>;
    } else if(groupResults.length == 0){
      return (
        <div id="LookUpHerdTempRangeWrapper"> 
          <span class="LookUpHerdNoData">No temperature data</span>
        </div>
     )
    } else{
      return (
        <div id="LookUpHerdTempRangeWrapper">
          <h1 className="cow-temp-range-heading">Temp - DEG C</h1>
          <ResponsiveContainer width="100%" height={370}>
          <LineChart data={groupResults}
                  margin={{top: 5, right: 30, left: 20, bottom: 5}}>
          < CartesianGrid strokeDasharray="3 3"/>
              <XAxis dataKey="dateTime"/>
              <YAxis/>
              <Tooltip/>
              <Legend />
              <Line dataKey="MAX" stroke="#FF0000" />
              <Line dataKey="MIN" stroke="#228B22" />
              <Line dataKey="AVG" stroke="#1E90FF" />
              
          </LineChart> 
          </ResponsiveContainer>
        </div>  
      );
    }
  }
}
export default LookUpHerdTempRange;


