import React, { Component } from 'react';
import { ReactBingmaps } from 'react-bingmaps';
import './LookUpHerdMap.css';

class LookUpHerdMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
          groupLoaded: false,
          groupResults: [],
        }
      }

      handlePinOnHover(event){
      }

    parseCowLocData(res){
        // Make the mouse over event for the pins display the date time of the pin and cow id
        let result = []
        res.forEach( response => {
          response.forEach(element => {
              if(element["loc_lat"] != null || element["loc_lon"] != null){
                  let pin = {
                      "location" : [
                          element["loc_lat"],
                          element["loc_lon"]
                      ],
                      "pushPinOption":{ title: 'Pushpin Title', description: 'Pushpin' },
                      "option" : {
                          "color" : "red"
                      },
                      "addHandler" : {
                          "type" : "mouseover",
                          callback : this.handlePinOnHover()
                      }
                  };
                  result.push(pin);
              }
          });
        })
        return result;
    }
      

    // fetches location for day with cow_id for a specified date
    componentDidMount(){
        this.setState({
            cowIdGroup: this.props.cowIdGroup,
            startDate: this.props.startDate,
            }, () => {
              let request = { 
              cowGroup: this.state.cowIdGroup
              }
              fetch((`${process.env.REACT_APP_API_URL}/grouplocday/` + this.props.startDate), {
                  method: 'post',
                  headers: {
                    'Content-Type': 'application/json', 
                    "Authorization" : 'Bearer '+ localStorage.getItem('token')
                    },
                  body: JSON.stringify(request)
              }) 
              .then(res => res.json())
              .then(json => {
                  if(json.response.length !== 0){
                      this.setState({
                          groupResults: this.parseCowLocData(json.response)
                      })
                  }
                  this.setState({
                      groupLoaded: true,
                  })
              });
            }
        )
    }

    render() {
        var {groupLoaded } = this.state;
        if (!groupLoaded) {
          return <div className="LookUpHerdLoading">Loading...Cow Location Data</div>;
        } else if(this.state.groupResults.length === 0) {
            return ( 
                <div id="LookUpHerdMap">
                    <span class="LookUpHerdNoData">No Location data </span>
                </div>
            )
        } else {
            return (
                <div className="LookUpHerdMap">
                    <h1 className="LookUpHerdMapHeading">Cow Map</h1>
                    <ReactBingmaps 
                        bingmapKey = "Al4i0oHFLW-z-hDROXL89Wp37vGOgqMJT6aX6ompovuUH9Ef54yzbBuGdyWhRtEe"
                        center = {[-35.0479, 147.312]}
                        mapTypeId= {"aerial"}
                        navigationBarMode={"minified"}
                        pushPins={this.state.groupResults}
                    />
                </div>
            )
        }
    }
}

export default LookUpHerdMap