import React, { Component } from 'react';
import LookUpHerdTempDay from '../LookUpHerdTempDay/LookUpHerdTempDay';
import CowDetails from '../../CowDetails/CowDetails';
import LookUpHerdActivityDay from '../LookUpHerdActivityDay/LookUpHerdActivityDay';
import LookUpHerdMap from '../LookUpHerdCowMap/LookUpHerdMap';
import './LookUpHerdDayDisplay.css';

class LookUpHerdDayDisplay extends Component {
    constructor(props){
        super(props)
        this.state = {
          cowIdGroup: null,
          groupStartDate: null,
    }
  }

  componentDidMount(){
    const { cowIdGroup } = this.props.location.state
    const { groupStartDate } = this.props.location.state
    this.setState({
      cowIdGroup: cowIdGroup,
      groupStartDate: this.formatDate(groupStartDate),
    })
  }

    formatDate(date){
        let formattedDate = date.toISOString()
        formattedDate = formattedDate.split("T")[0]
        return formattedDate
      }

    render() {
        if(this.state.cowIdGroup && this.state.groupStartDate){
            return (
                <div className="LookUpHerdDayDisplay">
                    <h1 className="LookUpHerdSearchTitle">Look Up Herd</h1>
                    <LookUpHerdActivityDay cowIdGroup={this.state.cowIdGroup} startDate={this.state.groupStartDate}/> 
                    <div className="LookUpHerdDayDisplayGraphWrapper">
                      <LookUpHerdTempDay cowIdGroup={this.state.cowIdGroup} startDate={this.state.groupStartDate} />
                      <LookUpHerdMap cowIdGroup={this.state.cowIdGroup} startDate={this.state.groupStartDate} /> 
                    </div>
                    <h2 className="LookUpHerdGroupTitle">Group Details:</h2>
                    <CowDetails items={this.state.cowIdGroup} />
                </div>
            )
        } else {
            return (
                <div className="LookUpHerdDayDisplay">
                    <h1 className="SearchTitle">Loading...</h1>
                </div>
            )
        }
    }
}

export default LookUpHerdDayDisplay