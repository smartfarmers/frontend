
import React from "react";
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';
import './LookUpHerdActivityRange.css';

class LookUpHerdActivityRange extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      groupLoaded: false,
      cowIdGroup: [],
      groupResults: [],
      amountOfDays: 0,
    }
  }

  componentDidMount(){
    this.setState({
      cowIdGroup: this.props.cowIdGroup,
      groupStartDate: this.props.groupStartDate,
      groupEndDate: this.props.groupEndDate,
    }, async () => {
      await this.getActRangeData(
        this.state.cowIdGroup, 
        this.state.groupStartDate, 
        this.state.groupEndDate
      );
    })
  }

  async getActRangeData(cowIdGroup, startDate, endDate){
    let daysDiff = this.getDifferenceInDates(startDate, endDate)
    for(let i = 0; i <= daysDiff; i++){
      let day = new Date();
      let addValue = 1;
      if( i == 0 ){
        addValue = 0
      }
      day = startDate.setDate(startDate.getDate() + addValue);
      day = new Date(day);
      day = this.formatDate(day)
      await cowIdGroup.forEach( async (element, index) => {
        fetch(`${process.env.REACT_APP_API_URL}/actday/` + element + '/' + day,
          {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}
        ) 
        .then(res => res.json())
        .then(json => {
          if(json.response !== null){
            if(json.response.length !== 0){
              if(index === 0){
                this.addToAverage(json.response, true);
              }
              this.addToAverage(json.response, false);
            }
            if(index === this.state.cowIdGroup.length - 1){
              this.averageData();
            }
          }
        }).then( () => {
          if(daysDiff === i){
            this.setState({
              groupLoaded: true
            }) 
          }
        })
      })
      addValue++
    }
  }

  getDifferenceInDates(startDate, endDate){
    let diff = endDate.getTime() - startDate.getTime();
    let diffInDays = diff / (1000 * 3600 * 24);
    return parseInt(diffInDays)
  }

  formatDate(date){
    let formattedDate = date.toISOString()
    formattedDate = formattedDate.split("T")[0]
    return formattedDate
  }

  addToAverage(response, firstResult){
    let res = this.parseActData(response);
    res.forEach((element, index)=> {
      if(firstResult){
        this.setState({
          groupResults: [...this.state.groupResults, {
            dateTime: element["dateTime"],
            resting: element["resting"],
            walking: element["walking"],
            grazing: element["grazing"]
          }]
        })
      } else {
        var newResults = this.state.groupResults;
        newResults[index].resting += element["resting"];
        newResults[index].walking += element["walking"];
        newResults[index].grazing += element["grazing"];
        this.setState({
          groupResults: newResults
        })
      }
    })
  }

  averageData(){
    var res;
    var groupCount;
    res = this.state.groupResults;
    groupCount = this.state.cowIdGroup.length;
    res.forEach( element => {
      element.grazing /= groupCount;
      element.resting /= groupCount;
      element.walking /= groupCount;
    })
    this.setState({
      group1Results: res
    })
  }

  parseActData(response){
    let result = []
    response.forEach(element => {
        let resting;
        let walking;
        let grazing;
        try{
            resting = element["hourSummaries"][0]["count"]
        } catch{
            resting = 0;
        }
        try{
            walking = element["hourSummaries"][1]["count"]
        } catch{
            walking = 0;
        }
        try{
            grazing = element["hourSummaries"][2]["count"]
        } catch{
            grazing = 0;
        }
        var dateTime;
        dateTime = {
          "dateTime" : `${element["day"]} - ${element["hour"]}`,
          "resting" : resting,
          "walking" : walking,
          "grazing" : grazing
        }
        result.push(dateTime)
    });
    return result;
  }
  
  render() {
    var { groupLoaded, groupResults } = this.state;
    if (!groupLoaded) {
      return <div className="LookUpHerdLoading">Loading... Activity Data</div>;
    } else if(groupResults.length === 0){
      return (
        <div id="LookUpHerdActivityRangeWrapper"> 
          <span class="LookUpHerdNoData">No Activity data</span>
        </div>
     )
    } else{
      return (
        <div id="LookUpHerdActivityRangeWrapper">
            <h1>Cow Activity</h1>
            <ResponsiveContainer width="100%" height={300}>
            <LineChart data={groupResults}
                    margin={{top: 5, right: 30, left: 20, bottom: 5}}>
            < CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="dateTime"/>
                <YAxis/>
                <Tooltip/>
                <Legend />
                <Line dataKey="resting" stroke="#1E90FF" />
                <Line dataKey="grazing" stroke="#228B22" />
                <Line dataKey="walking" stroke="#FF00FF" />
            </LineChart> 
            </ResponsiveContainer>
            </div>
      );
    }
  }
}
export default LookUpHerdActivityRange;
