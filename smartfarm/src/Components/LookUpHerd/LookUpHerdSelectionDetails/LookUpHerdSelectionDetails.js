import React, { Component } from "react";
import "./LookUpHerdSelectionDetails.css";
import LookUpHerdSingleDate from '../LookUpHerdDateDetails/LookUpHerdSingleDate';
import LookUpHerdRangeDate from '../LookUpHerdDateDetails/LookUpHerdRangeDate';
import MultiSelect from "@khanacademy/react-multi-select";

class LookUpHerdSelectionDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoaded: false,
        cowIdGroup: [],
        cowOptions: []
    }
    this.handleCheck = this.handleCheck.bind(this);
    this.remapCowList = this.remapCowList.bind(this);
  }

  componentDidMount(){
    fetch(`${process.env.REACT_APP_API_URL}/cowlist`, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
      .then(res => res.json())
      .then(json => {
       this.setState({
         isLoaded: true,
         cowList: json.response, 
       })
       this.remapCowList()
    }); 
  }

  remapCowList() {
    var res = []
    this.state.cowList.map((cow) => {
      res.push({
        label: cow.cow_id,
        value: cow.cow_id
      })
    })
    this.setState({
      cowOptions: res
    })
  }

  renderSelectionOrder() {
    if(this.state.cowIdGroup.length === 0){
      return(<span>Please select a group of CowIds</span>)
    } else { 
      return(<div></div>)
    }
  }

  handleCheck(event){
    if(event.target.value === "singleDay"){
      this.setState({dateSelector: "singleDay"})
    } else if(event.target.value === "range"){
      this.setState({dateSelector: "range"})
    }
  }

  renderDatePicker() {
    if(this.state.cowIdGroup.length === 0){
      return(<div></div>)
    } else {
      if(this.state.dateSelector === "singleDay"){
        return (
          <LookUpHerdSingleDate 
            cowIdGroup={this.state.cowIdGroup}
          />
        )
      } else if (this.state.dateSelector === "range") {
        return (
          <LookUpHerdRangeDate 
            cowIdGroup={this.state.cowIdGroup}
          />
        )
      }
    }
  }

  render() {
    const {cowIdGroup} = this.state;
    var {isLoaded } = this.state;
    if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
      <div className="LookUpHerdSelectionDetails">
        { this.renderSelectionOrder() }
        <form>
          <div>
            <label className="CowIdSelectionLabel">Select Cow Group:</label>
            <MultiSelect
              className="MultiCowSelect"
              options={this.state.cowOptions}
              selected={cowIdGroup}
              onSelectedChanged={cowIdGroup => this.setState({cowIdGroup})}
            />
          </div>
          <div className="DateRangeSelector">
            <label className="selectRangeLabel">Select Range:</label>
            <div className="LookUpHerdRangeSelectors">
              <label>
                <input
                  name="dateSelector"
                  value="singleDay"
                  id="LookUpHerdSingleDay"
                  type="radio"
                  className="SingleCheckBox"
                  checked={this.state.singleDay}
                  onChange={this.handleCheck} />
                  Single Day
              </label>
              <br />
              <label className="DateRangeCheckBoxLabel">
              <input
                name="dateSelector"
                value="range"
                type="radio"
                id="LookUpHerdRange"
                className="DateRangeCheckBox"
                checked={this.state.range}
                onChange={this.handleCheck}
              />
                  Date Range
              </label>
            </div>
          </div>
          <div className="DateDetails">
            { this.renderDatePicker() }    
          </div>
        </form>
        </div>
      );
    }
  }
}

export default LookUpHerdSelectionDetails;