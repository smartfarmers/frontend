import React, { Component } from 'react';
import { ReactBingmaps } from 'react-bingmaps';
import './LookUpHerdMapRange.css';

class LookUpHerdMapRange extends Component {
    constructor(props) {
        super(props);
        this.state = {
          groupLoaded: false,
          cowIdGroup: [],
          groupResults: [],
        }
      }

      handlePinOnHover(event){
          console.log("mouse Over: " + event)
      }

    // fetches location with cow_id for a specified date
    componentDidMount(){
      this.setState({
        cowIdGroup: this.props.cowIdGroup,
        groupStartDate: this.props.groupStartDate,
        groupEndDate: this.props.groupEndDate,
      }, async () => {
        await this.getLocRangeData(
          this.state.cowIdGroup,
          this.state.groupStartDate,
          this.state.groupEndDate,
        );
      })
    }

    async getLocRangeData(cowIdGroup,  groupStartDate, groupEndDate){
      let startDate = this.state.groupStartDate
      let endDate = this.state.groupEndDate
      startDate = this.formatDate(startDate)
      endDate = this.formatDate(endDate);

      let request = { 
        cowGroup: cowIdGroup
      }
      fetch((`${process.env.REACT_APP_API_URL}/grouplocrange/` + startDate + '/' + endDate), {
        method: 'post',
        headers: {
          'Content-Type': 'application/json', 
          "Authorization" : 'Bearer '+ localStorage.getItem('token')}, 
        body: JSON.stringify(request)
      }) 
        .then(res => res.json())
        .then(json => {
          if(json.response.length !== 0){
              this.setState({
                  groupResults: this.parseCowLocData(json.response)
              })
          }
          this.setState({
              groupLoaded: true,
          })
      });
    }
    
      formatDate(date){
        let formattedDate = date.toISOString()
        formattedDate = formattedDate.split("T")[0]
        return formattedDate
      }

      parseCowLocData(res){
        // Make the mouse over event for the pins display the date time of the pin and cow id
        let result = []
        res.forEach( response => {
          response.forEach(element => {
              if(element["loc_lat"] != null || element["loc_lon"] != null){
                  let pin = {
                      "location" : [
                          element["loc_lat"],
                          element["loc_lon"]
                      ],
                      "pushPinOption":{ title: 'Pushpin Title', description: 'Pushpin' },
                      "option" : {
                          "color" : "red"
                      },
                      "addHandler" : {
                          "type" : "mouseover",
                          callback : this.handlePinOnHover()
                      }
                  };
                  result.push(pin);
              }
          });
        })
        return result;
    }

    render() {
      var {groupLoaded } = this.state;
      if (!groupLoaded) {
          return <div className="LookUpHerdLoading">Loading... Location Data</div>;
      } else if(this.state.groupResults.length === 0){
          return (
            <div id="LookUpHerdMapRange"> 
              <span className="LookUpHerdNoData">No Location data</span>
            </div>
         )
      } else {
        return (
          <div className="LookUpHerdMapRange">
              <h1 className="CowMapHeading">CowMap</h1>
              <ReactBingmaps 
                  bingmapKey = "Al4i0oHFLW-z-hDROXL89Wp37vGOgqMJT6aX6ompovuUH9Ef54yzbBuGdyWhRtEe"
                  center = {[-35.0479, 147.312]}
                  mapTypeId= {"aerial"}
                  navigationBarMode={"minified"}
                  pushPins={this.state.groupResults}
              />
          </div>
        )
      }
    }
}

export default LookUpHerdMapRange