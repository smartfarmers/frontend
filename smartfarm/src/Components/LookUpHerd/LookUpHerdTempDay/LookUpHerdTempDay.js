import React from "react";
import './LookUpHerdTempDay.css';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,} from 'recharts';

class LookUpHerdTempDay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      groupLoaded: false,
      groupResults: [],
    }
  }

  // fetches temperature with cow_id for a specified date
  componentDidMount(){
    this.setState({
      cowIdGroup: this.props.cowIdGroup,
      startDate: this.props.startDate,
    }, () => {
        let request = { 
          cowGroup: this.state.cowIdGroup
        }
        fetch((`${process.env.REACT_APP_API_URL}/grouptempdate/` + this.props.startDate), {
          method: 'post',
          headers: {
            'Content-Type': 'application/json',
            "Authorization" : 'Bearer '+ localStorage.getItem('token')},
          body: JSON.stringify(request)
        }) 
        .then(res => res.json())
        .then(json => {
          if(json.response.length !== 0){
            this.setState({
              groupResults: json.response
            })
          } else {
            this.setState({
              groupResults: [{
                '00_00': 0,
                '04_00': 0,
                '08_00': 0,
                '12_00': 0,
                '16_00': 0,
                '20_00': 0,
                MAX: 0,
                TEMP_DATE: this.props.startDate
              }],
              noGroupResults: true
            })
          }
          this.setState({
            groupLoaded: true, 
          })
        }); 
      }
    )
  }
  
  render() {
    var {groupLoaded, groupResults } = this.state;
    if (!groupLoaded) {
      return <div className="LookUpHerdLoading">Loading... temperature data</div>;
    } else if(this.state.noGroupResults) {
      return (
        <div className="LookUpHerdNoData">No Temperature data</div>
      )
    }
    else{
      return (
        <div id="LookUpHerdTempDayWrapper"> 
          <div >
            <h1 className="cow-temp-heading">Temp - DEG C</h1>
            <BarChart width={550} height={380} data={[
                {'name': 'Max', 'Herd': groupResults[0].MAX},
                {'name': '00_00', 'Herd': groupResults[0]["00_00"]},
                {'name': '04_00', 'Herd': groupResults[0]["04_00"]},
                {'name': '08_00', 'Herd': groupResults[0]["08_00"]},
                {'name': '12_00', 'Herd': groupResults[0]["12_00"]},
                {'name': '16_00', 'Herd': groupResults[0]["16_00"]},
                {'name': '20_00', 'Herd': groupResults[0]["20_00"]},
              ]} 
                margin={{top: 20, right: 30, left: 10, bottom: 10}}>
                
            < CartesianGrid strokeDasharray="3 3"/>
              <XAxis dataKey="name"/>
              <YAxis/>
              <Tooltip/>
              <Legend />
            <Bar dataKey="Herd" fill="#FF0000" />
            </BarChart> 
          </div> 
        </div>  
      );
    }
  }
}
export default LookUpHerdTempDay;


