import React, { Component } from 'react';
import './LookUpHerd.css';
import LookUpHerdSelectionDetails from './LookUpHerdSelectionDetails/LookUpHerdSelectionDetails';
import { Redirect } from 'react-router-dom';

class LookUpHerd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            cowId: '',
        }
      }

    render() {
        if (!localStorage.getItem('token')) {
            return (
                <Redirect to={{ pathname: '/' }} push/>
            ) 
        }
        return (
            <div className="LookUpHerd">
                <div className="LookUpHerdCard">  
                    <h1 className="SearchTitle">Look Up Herd</h1>
                    <LookUpHerdSelectionDetails />
                </div>
            </div>
        )
    }
}

export default LookUpHerd