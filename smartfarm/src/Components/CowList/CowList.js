import React from "react";
import Table from "../CommonComponents/Table/Table";
import { BrowserRouter as Router, Route } from "react-router-dom";
import CowDetails from '../CowDetails/CowDetails';

class CowList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [], // for cow id & collar id
      isLoaded: false,
      cowId: ''
    }
    this.headers = ["Cow id", "Collar id"]
  }

  componentDidMount(){
    fetch(`${process.env.REACT_APP_API_URL}/cowlist/`, {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) // fetches a list of cows and their current collar
       .then(res => res.json())
       .then(json => {
          this.setState({
            isLoaded: true,
            items: json, 
          }) 
      }); 
  }
 
  handleTableDataClick(event) {
    const value = event.target.textContent;
    console.log(value)
    clickOn(value);
  }

  render() {
    var {isLoaded, items } = this.state;

      if (!isLoaded) {
      return <div>Loading...</div>;
    }
    else{
      return (
        <div>      
          {items.response.map(item => ( 
            <div key={item.id} onClick={this.handleTableDataClick}>
              <Table 
                headers={this.headers} 
                tableData={this.tableData = [[item.cow_id, item.collar_id]]}
              /> 
            </div>
          ))}
        </div>  
      );
    }
  }
}

function clickOn(e) {
  return (
    <React.Fragment>
      <Router>
        <div>         
            <Route path="./CowDetails/" component={() => <CowDetails cowId={e} />} />                    
        </div>
      </Router>
    </React.Fragment>
  )
  
}

export default CowList;
