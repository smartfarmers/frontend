import React, { Fragment, Component } from 'react';
import Error from '../Errors/Error'
import { Redirect } from 'react-router-dom';
import './Login.css';

class Login extends Component {

  constructor(props){
    super(props);

    this.state = {
        username:'',
        password:'',
        loginResponse: null,
        loggedIn: false,
        getError: null,
        loginError: null        
    }

    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInput(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value });      
  }

  handleSubmit(event) {
    let login = {
      username: this.state.username,
      password: this.state.password
    }
    event.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/login`, {
      method: 'post',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(login)
    }).then(res => res.json())
      .then(json => {
        if(json.status === "200" ){     
          localStorage.setItem('token', json.token)
          this.setState({
            loginResponse: json,
            loggedIn: true    
          })
		    } else {
          this.setState({
            loginError: json
          })
        }              
    })
    .catch(error => {
        this.setState({
          getError: error
        })
    });    
  }

  getError(){
		if(this.state.getError){
		  return (
			<Error
			  errorType="error"
			  errorHeading="Unable to reach server" 
			  errorMessage="We were unable to connect to server for validation, please contact application administrator">
			</Error>
		  )
		}
  }
  
  loginError(){
		if(this.state.loginError){
      if(this.state.loginError.msg === "Password is incorrect") {
        return (
          <Error
            errorType="error"
            errorHeading="Incorrect password" 
            errorMessage="Incorrect password">
          </Error>
          )
      }
      else {
        return (
          <Error
            errorType="error"
            errorHeading="User not found" 
            errorMessage="User not found">
          </Error>
        )
      }
		  
		}
	}

  render () {       
    if (this.state.loggedIn) {
     return <Redirect to={{ pathname: '/Cow' }} push/>
    }
    if (localStorage.getItem('token')) {
      return (
        <Redirect to={{ pathname: '/Cow' }} push/>
      ) 
  }                      
    return (
      <div className="container">        
          <Fragment>
              <h1>Login</h1>
              <form>
              <h6>Username:</h6>                
              <input type="login-text" value={this.state.username} class={"textbox"} name="username" onChange={this.handleInput} />              
              <h6>Password:</h6>               
              <input type="password" value={this.state.password} class={"textbox"} name = "password" onChange={this.handleInput} />              
              <button type={"login"} class={"LoginButton"} onClick={this.handleSubmit} disabled={!this.state.password || !this.state.username}>  Submit </button>
              </form>
          </Fragment>
          <div className="flex-row login-errors">
				    { this.getError() }
            { this.loginError() }
        	</div>            
      </div>
      )
  }
}

export default Login;