import React from "react";
import './CowBattRange.css';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';

class CowBattRange extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      battData: [],
      isLoaded: false,
      cowId: null,
      startDate: null,
      endDate: null
    }
  }

  // fetches battery voltage with cow_id for a specified date range
  componentDidMount(){
    this.setState({
      cowId: this.props.cowId,
      startDate: this.props.startDate,
      endDate: this.props.endDate
    })

    let startDate = this.formatDate(this.props.startDate);
    let endDate = this.formatDate(this.props.endDate);
    fetch(`${process.env.REACT_APP_API_URL}/battrange/` + this.props.cowId + '/' + startDate + '/' + endDate, 
    {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
      .then(res => res.json())
      .then(json => {
       this.setState({
         isLoaded: true,
         battData: this.parseBatData(json), 
       }) 
    }); 
  }

  formatDate(date){
    let formattedDate = date.toISOString()
    formattedDate = formattedDate.split("T")[0]
    return formattedDate
  }

  parseBatData(battData){
    let wrapperArray = [];
    battData.response.forEach(element => {
      let MIN;
      let MAX;
      let AVG;
      try{
        MIN = element[1][0]["MIN"]
      } catch{
        MIN = 0;
      }
      try{
        MAX = element[1][1]["MAX"]
      } catch{
        MAX = 0;
      }
      try{
        AVG = element[1][2]["AVG"]
      } catch{
        AVG = 0;
      }

      let result = {
        "dateTime" : `${element[0]}`,
        "MIN" : MIN,
        "MAX" : MAX,
        "AVG" : AVG
      }
      wrapperArray.push(result)
    });
    return wrapperArray
  }
  
  render() {
    var {isLoaded, battData } = this.state;
    if (!isLoaded) {
      return <div className="LookUpCowLoading">Loading...</div>;
    } else if(battData.length === 0){
      return <div className="LookUpCowNoData">No Battery data for this date range</div>
    } else{
      return (
        <div id="CowBattRangeWrapper">
          <h3 className="CowBattRangeHeader">Batt Volt - mV</h3>
          <ResponsiveContainer width="100%" height={280}>
            <LineChart data={battData}
              margin={{top: 5, right: 30, left: 20, bottom: 5}}>
              < CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="dateTime"/>
                <YAxis/>
                <Tooltip/>
                <Legend />
                <Line dataKey="MAX" stroke="#FF0000" />
                <Line dataKey="MIN" stroke="#228B22" />
                <Line dataKey="AVG" stroke="#1E90FF" />
            </LineChart> 
          </ResponsiveContainer>
        </div>  
      );
    }
  }
}
export default CowBattRange;


