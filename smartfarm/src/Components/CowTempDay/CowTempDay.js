import React from "react";
import './CowTempDay.css';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,} from 'recharts';

class CowTempDay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tempDay: [],
      isLoaded: false,
      cowId: '',
      date:''
    }
  }

  // fetches temperature with cow_id for a specified date
  componentDidMount(){
    fetch(`${process.env.REACT_APP_API_URL}/tempdate/` + this.props.cowId + '/'+ this.props.date,
     {headers: {"Authorization" : 'Bearer '+ localStorage.getItem('token')}}) 
      .then(res => res.json())
      .then(json => {
       this.setState({
         isLoaded: true,
         tempDay: json, 
       }) 
    }); 
  }
  
  render() {
    var {isLoaded, tempDay } = this.state;
    if (!isLoaded) {
      return <div className="LookUpCowLoading">Loading</div>;
    } else if(tempDay.response.length === 0){
        return (
          <div className="LookUpCowNoData">No Temperature for this date</div>
        )
    } else{
      return (
        <div id="CowTempDayWrapper"> 
        {tempDay.response.map(item => ( 
            <div key={item.id}>
              <h3 className="CowTempDayHeader">Temp - DEG C</h3>
              <BarChart width={450} height={280} data={[
                  {'name': 'Max', 'reading': item.TEMP_MAX,},
                  {'name': '00_00', 'reading': item.TEMP_00_00,},
                  {'name': '04_00', 'reading': item.TEMP_04_00,},
                  {'name': '08_00', 'reading': item.TEMP_08_00,},
                  {'name': '12_00', 'reading': item.TEMP_12_00,},
                  {'name': '16_00', 'reading': item.TEMP_16_00,},
                  {'name': '20_00', 'reading': item.TEMP_20_00,},
                ]} 
                  margin={{top: 20, right: 30, left: 10, bottom: 10}}>
                  
              < CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="name"/>
                <YAxis/>
                <Tooltip/>
                <Legend />
              <Bar dataKey="reading" fill="#e63946" />
              </BarChart> 
            </div> 
           ))}
        </div>  
      );
    }
  }
}
export default CowTempDay;


