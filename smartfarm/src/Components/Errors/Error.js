import React from "react";
import './Error.css';

class Error extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        errorType: props.errorType,
        errorHeading: props.errorHeading,
        errorMessage: props.errorMessage
    }
  }

  
  componentDidMount(){

  }
  
  

  render() {
    if(this.state.errorType === "error"){
      return (
        <div className="error-Wrapper wrapper">
          <h2 className="error-Heading">Error: {this.state.errorHeading}</h2>
          <p className="error-Text">{this.state.errorMessage}</p>
        </div>  
      );
    } else if(this.state.errorType === "warning") {
      return (
        <div className="warning-Wrapper wrapper">
          <h2 className="warning-Heading">Warning: {this.state.errorHeading}</h2>
          <p className="warning-Text">{this.state.errorMessage}</p>
        </div>  
      );
    } else if(this.state.errorType === "information") {
      return (
        <div className="information-Wrapper wrapper">
          <h2 className="information-Heading">Info: {this.state.errorHeading}</h2>
          <p className="information-Text">{this.state.errorMessage}</p>
        </div>  
      );
    }
  }
}
export default Error;

