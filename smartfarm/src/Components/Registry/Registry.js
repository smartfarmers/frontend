import React, { Fragment, Component } from 'react';
import Error from '../Errors/Error'
import { Redirect } from 'react-router-dom'; 
import './Registry.css';

class Registry extends Component {

    constructor(props){
        super(props);
          
        this.state = {
            username:'',
            password:'',
            registerResponse: null,
            userCreated: false,
            postError: null,
            getError: null,
            userAdded: null        
        }
    
        this.handleInput = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleToggle = this.handleToggle.bind(this);
      }

    handleInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value });      
    }


    handleToggle(e) {
      this.setState ({
        username:'',
        password:'',
        postError: null,
        getError: null,
        userCreated: false
      })
    }

    handleSubmit(event) {
        let user = {
          username: this.state.username,
          password: this.state.password
        }
        console.log(JSON.stringify(user))
        event.preventDefault();
        fetch(`${process.env.API_URL}/register`, {
      method: 'post',
      headers: {'Content-Type': 'application/json', "Authorization" : 'Bearer '+ localStorage.getItem('token')},
      body: JSON.stringify(user)
    }).then(res => res.json())
      .then(json => {
        console.log(json)
        if(json.status !== "200" ) {
          this.setState({
            postError: json
          })
        } else {
          console.log('or not')    
          this.setState({
            registerReponse: json,
            userCreated: true
          })
		    }         
    }).catch(error => {
        this.setState({
          getError: error
        })
    }); 
    }

    userAdded() {
      if(this.state.userCreated){
        return (
          <Fragment>
            <h1>User has been added</h1>
            <button type="redirect" onClick={this.handleToggle}>Add another user</button>
          </Fragment>
        )
      }
    }

    getError(){
      if(this.state.getError){
        return (
        <Error
          errorType="error"
          errorHeading="Unable to reach server" 
          errorMessage="We were unable to connect to server for validation, please contact application administrator">
        </Error>
        )
      }
    }

    postError(){
      if(this.state.postError){
        return (
          <Error
            errorType="error"
            errorHeading="Validation Error" 
            errorMessage="Username is a duplicate or invalid, try again">
          </Error>
        )}
    }

    render () {
        if (!localStorage.getItem('token')) {
            return (
                <Redirect to={{ pathname: '/' }} push/>
            ) 
        } 
        return (          
            
          <div className="container">
              <Fragment>
              <h1>Create New User</h1>
              <h2>Enter a username and password</h2> 
              <form>
              
              <h6>Username:</h6>  
              <input type="register-text" value={this.state.username} class={"textbox"} name="username" onChange={this.handleInput} />
              
              
              <h6>Password:</h6>  
                <input type="password" value={this.state.password} class={"textbox"} name = "password" onChange={this.handleInput} />
              
              <button type={"register"} class={"RegSubmitButton"} onClick={this.handleSubmit} disabled={!this.state.password || !this.state.username}>  Submit </button>
              </form> </Fragment>
                       
              <div className="user-created">
                { this.userAdded() }
              </div>
          
              <div className="flex-row login-errors">
              { this.getError() }
              { this.postError() }
            </div>
          </div>  
        )
    }
}

export default Registry